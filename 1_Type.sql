--------------------------------------------------------------------------------
-- Destructeur de types :D
--
begin
  for r in (select type_name from user_types) loop
    execute immediate 'drop type ' || r.type_name || ' force';
  end loop;
end;
/


--------------------------------------------------------------------------------
-- adresse														adresse_t
--
CREATE OR REPLACE TYPE adresse_t AS OBJECT
(
	numRue INT,
	nomRue VARCHAR2(50),
	complement VARCHAR2(50),
	codeP NUMBER(5),
	ville VARCHAR2(50)
);
/


/*******************************************************************************
 * CLASSE document et CLASSES HERITANT de document
 * 	DOCUMENT 	-> LIVRE (herite de document)
 * 				-> MEDIA (herite de document) 	-> CD		(herite de media)
 *												-> DVD		(herite de media)
 *												-> VIDEO	(herite de media)
 */

--------------------------------------------------------------------------------
-- document														document_t
--
CREATE OR REPLACE TYPE document_t AS OBJECT
(
	id INT,
	titre VARCHAR2(100),
	theme VARCHAR2(50),
	nbrExpl INT,

--FINAL INSTANTIABLE
	FINAL MEMBER FUNCTION getType RETURN VARCHAR2,
	NOT FINAL MEMBER FUNCTION getDuree RETURN INT,
	NOT FINAL MEMBER FUNCTION getNbrPages RETURN INT,
	NOT FINAL MEMBER FUNCTION getNbrSsTitre RETURN INT,
	NOT FINAL MEMBER FUNCTION getFormate RETURN VARCHAR2
) NOT FINAL;
/
CREATE OR REPLACE TYPE ref_document AS OBJECT (r REF document_t);
/
CREATE OR REPLACE TYPE ens_ref_document AS TABLE OF ref_document;
/


--------------------------------------------------------------------------------
-- media														media_t
--
CREATE TYPE media_t UNDER document_t (
	duree INT,
	OVERRIDING MEMBER FUNCTION getDuree RETURN INT
) NOT FINAL;
/


--------------------------------------------------------------------------------
-- livre														livre_t
--
CREATE OR REPLACE TYPE livre_t UNDER document_t (
	nbrPages INT,
	OVERRIDING MEMBER FUNCTION getNbrPages RETURN INT
);
/


--------------------------------------------------------------------------------
-- cd															cd_t
--
CREATE OR REPLACE TYPE cd_t UNDER media_t (
	nbrSsTitre INT,
	OVERRIDING MEMBER FUNCTION getNbrSsTitre RETURN INT
);
/


--------------------------------------------------------------------------------
-- video														video_t
--
CREATE OR REPLACE TYPE video_t UNDER media_t (
	formate VARCHAR(10),
	OVERRIDING MEMBER FUNCTION getFormate RETURN VARCHAR2
);
/


/*******************************************************************************
 * CLASSE personne et CLASSES HERITANT de personne
 * 	 PERSONNE 	-> EDITEUR 		(herite de personne)
 * 				-> AUTEUR		(herite de personne)
 *				-> EMPRUNTEUR	(herite de personne)
 */

--------------------------------------------------------------------------------
-- personne														personne_t
--
CREATE OR REPLACE TYPE personne_t AS OBJECT
(
	id INT,
	nom VARCHAR2(20),
	numTel NUMBER(10)
) NOT FINAL;
/


--------------------------------------------------------------------------------
-- editeur														editeur_t
--
CREATE OR REPLACE TYPE editeur_t UNDER personne_t
(
	adresse adresse_t,
	documents ens_ref_document
);
/


--------------------------------------------------------------------------------
-- auteur														auteur_t
--
CREATE OR REPLACE TYPE auteur_t UNDER personne_t
(
	prenom VARCHAR2(20),
	dateN DATE,
	documents ens_ref_document
);
/
CREATE OR REPLACE TYPE ref_auteur AS OBJECT (r REF auteur_t);
/
CREATE OR REPLACE TYPE ens_ref_auteur AS TABLE OF ref_auteur;
/


--------------------------------------------------------------------------------
-- emprunteur													emprunteur_t
--
CREATE TYPE emprunteur_t UNDER personne_t
(
	prenom VARCHAR2(20),
	adresse adresse_t
);
/
CREATE TYPE ref_emprunteur AS OBJECT (r REF emprunteur_t);
/
CREATE TYPE ens_ref_emprunteur AS TABLE OF ref_emprunteur;
/


/*******************************************************************************
 * AUTRES CLASSES :
 * -> mot clé						MOT_CLE
 * -> exemplaire					EXEMPLAIRE
 * -> catégorie de l'emprunteur		CATEGORIE_EMPR
 * -> emprunt						EMPRUNT
 */

--------------------------------------------------------------------------------
-- mot clé														mot_cle_t
--
CREATE TYPE mot_cle_t AS OBJECT
(
	id INT,
	nom VARCHAR2(50),
	documents ens_ref_document
);
/
CREATE TYPE ref_mot_cle AS OBJECT (r REF mot_cle_t);
/
CREATE TYPE ens_ref_mot_cle AS TABLE OF ref_mot_cle;
/


--------------------------------------------------------------------------------
-- exemplaire													exemplaire_t
--
CREATE TYPE exemplaire_t AS OBJECT
(
	id INT,
	numRayon INT,
	document REF document_t
);
/
CREATE TYPE ref_exemplaire AS OBJECT (r REF exemplaire_t);
/
CREATE TYPE ens_ref_exemplaire AS TABLE OF ref_exemplaire;
/


--------------------------------------------------------------------------------
-- categorie de l'emprunteur									categorie_empr_t
--
CREATE TYPE categorie_empr_t AS OBJECT
(
	id INT,
	nom VARCHAR2(20),
	nbMaxEmprunt INT,
	dureeLivre INT,
	dureeCD INT,
	dureeDVD INT,
	dureeVideo INT,
	emprunteurs ens_ref_emprunteur
);
/


--------------------------------------------------------------------------------
-- emprunt														emprunt_t
--
CREATE TYPE emprunt_t AS OBJECT
(
	id INT,
	dateEmprunt DATE,
	dateRendu DATE,
	emprunteur REF emprunteur_t,
	exemplaire REF exemplaire_t
);
/
CREATE TYPE ref_emprunt AS OBJECT (r REF emprunt_t);
/
CREATE TYPE ens_ref_emprunt AS TABLE OF ref_emprunt;
/
