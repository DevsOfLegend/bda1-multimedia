--------------------------------------------------------------------------------
-- document														document_t
--
ALTER TYPE document_t ADD ATTRIBUTE editeur REF editeur_t CASCADE;
/
ALTER TYPE document_t ADD ATTRIBUTE auteurs ens_ref_auteur CASCADE;
/
ALTER TYPE document_t ADD ATTRIBUTE motsCles ens_ref_mot_cle CASCADE;
/
ALTER TYPE document_t ADD ATTRIBUTE exemplaires ens_ref_exemplaire CASCADE;
/


--------------------------------------------------------------------------------
-- categorie de l'emprunteur									categorie_empr_t
--
ALTER TYPE emprunteur_t ADD ATTRIBUTE categorie REF categorie_empr_t CASCADE;
/
ALTER TYPE emprunteur_t ADD ATTRIBUTE emprunts ens_ref_emprunt CASCADE;
/


--------------------------------------------------------------------------------
-- exemplaire													exemplaire_t
--
ALTER TYPE exemplaire_t ADD ATTRIBUTE emprunts ens_ref_emprunt CASCADE;
/
