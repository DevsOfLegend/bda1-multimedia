/*******************************************************************************
* @brief Ecriture des fonctions membres
*/
CREATE OR REPLACE TYPE BODY document_t AS
	/***************************************************************************
	* @brief Permet d'obtenir le type d'un document.
	* @return LIVRE|CD|DVD|VIDEO|DOC
	*
	* Utilisation:
	* @code
	*	SELECT * FROM exemplaire e WHERE e.document.getType() = 'LIVRE';
	* @endcode
	*/
	FINAL MEMBER FUNCTION getType RETURN VARCHAR2
	IS
	BEGIN
		IF SELF IS OF(livre_t) THEN
			RETURN 'LIVRE';
		END IF;
		IF SELF IS OF(cd_t) THEN
			RETURN 'CD';
		END IF;
		IF SELF IS OF(video_t) THEN
			RETURN 'VIDEO';
		END IF;
		IF SELF IS OF(media_t) THEN
			RETURN 'DVD';
		END IF;
		RETURN 'DOC';
	END;

	/***************************************************************************
	* @brief Création des accèsseurs aux données filles
	*/
	NOT FINAL MEMBER FUNCTION getDuree RETURN INT
	IS
	BEGIN
		RETURN NULL;
	END;

	NOT FINAL MEMBER FUNCTION getNbrPages RETURN INT
	IS
	BEGIN
		RETURN NULL;
	END;

	NOT FINAL MEMBER FUNCTION getNbrSsTitre RETURN INT
	IS
	BEGIN
		RETURN NULL;
	END;

	NOT FINAL MEMBER FUNCTION getFormate RETURN VARCHAR2
	IS
	BEGIN
		RETURN NULL;
	END;
END;
/


/*******************************************************************************
* @brief Permet d'obtenir la durée d'un media
* @return INT OR NULL
*
* Utilisation:
* @code
*	SELECT e.document.getDuree() FROM exemplaire e;
* @endcode
*/
CREATE OR REPLACE TYPE BODY media_t AS
	OVERRIDING MEMBER FUNCTION getDuree RETURN INT
	IS
	BEGIN
		RETURN duree;
	END;
END;
/


/*******************************************************************************
* @brief Permet d'obtenir le nombre de page d'un livre
* @return INT OR NULL
*
* Utilisation:
* @code
*	SELECT e.document.getNbrPages() FROM exemplaire e;
* @endcode
*/
CREATE OR REPLACE TYPE BODY livre_t AS
	OVERRIDING MEMBER FUNCTION getNbrPages RETURN INT
	IS
	BEGIN
		RETURN nbrPages;
	END;
END;
/


/*******************************************************************************
* @brief Permet d'obtenir le nombre de sous titre d'un CD
* @return INT OR NULL
*
* Utilisation:
* @code
*	SELECT e.document.getNbrSsTitre() FROM exemplaire e;
* @endcode
*/
CREATE OR REPLACE TYPE BODY cd_t AS
	OVERRIDING MEMBER FUNCTION getNbrSsTitre RETURN INT
	IS
	BEGIN
		RETURN nbrSsTitre;
	END;
END;
/


/*******************************************************************************
* @brief Permet d'obtenir le format d'une vidéo
* @return VARCHAR2 OR NULL
*
* Utilisation:
* @code
*	SELECT e.document.getFormate() FROM exemplaire e;
* @endcode
*/
CREATE OR REPLACE TYPE BODY video_t AS
	OVERRIDING MEMBER FUNCTION getFormate RETURN VARCHAR2
	IS
	BEGIN
		RETURN formate;
	END;
END;
/
