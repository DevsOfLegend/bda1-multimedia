--------------------------------------------------------------------------------
-- Destructeur de tables :D
--
begin
  for r in (select table_name from user_all_tables where table_name in ('EMPRUNT','CATEGORIE_EMPR','EXEMPLAIRE','MOT_CLE','EMPRUNTEUR','AUTEUR','EDITEUR','VIDEO','DVD','CD','LIVRE')) loop
    execute immediate 'drop table ' || r.table_name || ' cascade constraints';
  end loop;
end;
/

begin
  for r in (select object_name from user_objects where object_name in ('EMPRUNT','CATEGORIE_EMPR','EXEMPLAIRE','MOT_CLE','EMPRUNTEUR','AUTEUR','EDITEUR','VIDEO','DVD','CD','LIVRE')) loop
    execute immediate 'drop table ' || r.object_name || ' cascade constraints';
  end loop;
end;
/


--------------------------------------------------------------------------------
-- livre														LIVRE
--
CREATE TABLE livre OF livre_t
(
	PRIMARY KEY(id),

	CHECK( nbrPages >  0 ),
	CHECK( nbrExpl > 0),

	CHECK( nbrPages is not null ),
	CHECK( titre is not null ),
	CHECK( theme is not null ),
	CHECK( nbrExpl is not null  ),
	CHECK( editeur is not null ),
	CHECK( auteurs is not null ),
	CHECK( motsCles is not null ),
	CHECK( exemplaires is not null )
)
NESTED TABLE motsCles STORE AS LIVRE_Tab_Ref_mot_cle,
NESTED TABLE exemplaires STORE AS LIVRE_Tab_Ref_exemplaire,
NESTED TABLE auteurs STORE AS LIVRE_Tab_Ref_auteur;
/


--------------------------------------------------------------------------------
-- cd															CD
--
CREATE TABLE cd OF cd_t
(
	PRIMARY KEY (id),

	CHECK( nbrExpl > 0 ),
	CHECK( duree > 0 ),

	CHECK( nbrSsTitre is not null ),
	CHECK( duree is not null ),
	CHECK( titre is not null ),
	CHECK( theme is not null ),
	CHECK( nbrExpl is not null ),
	CHECK( editeur is not null ),
	CHECK( auteurs is not null ),
	CHECK( motsCles is not null ),
	CHECK( exemplaires is not null )
)
NESTED TABLE motsCles STORE AS CD_Tab_Ref_mot_cle,
NESTED TABLE exemplaires STORE AS CD_Tab_Ref_exemplaire,
NESTED TABLE auteurs STORE AS CD_Tab_Ref_auteur;
/


--------------------------------------------------------------------------------
-- dvd															DVD
--
CREATE TABLE dvd OF media_t
(
	PRIMARY KEY (id),

	CHECK( nbrExpl > 0 ),
	CHECK( duree > 0 ),

	CHECK( duree is not null ),
	CHECK( titre is not null ),
	CHECK( theme is not null ),
	CHECK( nbrExpl is not null ),
	CHECK( editeur is not null ),
	CHECK( auteurs is not null ),
	CHECK( motsCles is not null ),
	CHECK( exemplaires is not null )

)
NESTED TABLE motsCles STORE AS DVD_Tab_Ref_mot_cle,
NESTED TABLE exemplaires STORE AS DVD_Tab_Ref_exemplaire,
NESTED TABLE auteurs STORE AS DVD_Tab_Ref_auteur;
/


--------------------------------------------------------------------------------
-- video														VIDEO
--
CREATE TABLE video OF video_t
(
	PRIMARY KEY (id),

	CHECK( nbrExpl > 0 ),
	CHECK( duree > 0 ),
	CHECK( formate IN ('PAL','SECAM','AVI','MPEG','DIVX') ),

	CHECK( formate is not null ),
	CHECK( duree is not null ),
	CHECK( titre is not null ),
	CHECK( theme is not null ),
	CHECK( nbrExpl is not null ),
	CHECK( editeur is not null ),
	CHECK( auteurs is not null ),
	CHECK( motsCles is not null ),
	CHECK( exemplaires is not null )
)
NESTED TABLE motsCles STORE AS VIDEO_Tab_Ref_mot_cle,
NESTED TABLE exemplaires STORE AS VIDEO_Tab_Ref_exemplaire,
NESTED TABLE auteurs STORE AS VIDEO_Tab_Ref_auteur;
/


--------------------------------------------------------------------------------
-- editeur														EDITEUR
--
CREATE TABLE editeur OF editeur_t
(
	PRIMARY KEY (id),

	CHECK( adresse is not null ),
	CHECK( documents is not null ),
	CHECK( nom is not null ),
	CHECK( numTel is not null )
) NESTED TABLE documents STORE AS EDITEUR_Tab_Ref_document;
/


--------------------------------------------------------------------------------
-- auteur														AUTEUR
--
CREATE TABLE auteur OF auteur_t
(
	PRIMARY KEY (id),

	--TRIGGER
	--CHECK( TO_NUMBER(TO_CHAR(dateN,'yyyymmdd')) < TO_NUMBER(TO_CHAR(SYSDATE,'yyyymmdd')) ),

	CHECK( prenom is not null ),
	CHECK( dateN is not null ),
	CHECK( documents is not null ),
	CHECK( nom is not null ),
	CHECK( numTel is not null )
) NESTED TABLE documents STORE AS AUTEUR_Tab_Ref_document;
/


--------------------------------------------------------------------------------
-- emprunteur													EMPRUNTEUR
--
CREATE TABLE emprunteur OF emprunteur_t
(
	PRIMARY KEY (id),

	CHECK( prenom is not null ),
	CHECK( adresse is not null ),
	CHECK( nom is not null ),
	CHECK( numTel is not null ),
	CHECK( categorie is not null )
) NESTED TABLE emprunts STORE AS EMPRUNTEUR_Tab_Ref_emprunt;
/


/*******************************************************************************
 * AUTRES CLASSES :
 * -> mot clé						MOT_CLE
 * -> exemplaire					EXEMPLAIRE
 * -> catégorie de l'emprunteur		CATEGORIE_EMPR
 * -> emprunt						EMPRUNT
 */

--------------------------------------------------------------------------------
-- mot clé														MOT_CLE
--
CREATE TABLE mot_cle OF mot_cle_t
(
	PRIMARY KEY (id),
	UNIQUE(nom),

	CHECK( nom is not null ),
	CHECK( documents is not null )
) NESTED TABLE documents STORE AS MOTCLE_Tab_ref_document;
/


--------------------------------------------------------------------------------
-- exemplaire													EXEMPLAIRE
--
CREATE TABLE exemplaire OF exemplaire_t
(
	PRIMARY KEY (id),

	CHECK( numRayon > 0 ),

	CHECK( numRayon is not null ),
	CHECK( document is not null )
) NESTED TABLE emprunts STORE AS EXEMPLAIRE_Tab_Ref_emprunt;
/


--------------------------------------------------------------------------------
-- categorie de l'emprunteur									CATEGORIE_EMPR
--
CREATE TABLE categorie_empr OF categorie_empr_t
(
	PRIMARY KEY (id),

	CHECK( nom='personnel' OR nom='professionnel' OR nom='public' ),
	CHECK( nbMaxEmprunt > 0 ),
	CHECK( dureeLivre > 0 ),
	CHECK( dureeCD > 0 ),
	CHECK( dureeDVD > 0 ),
	CHECK( dureeVideo > 0 ),

	CHECK( nom is not null ),
	CHECK( nbMaxEmprunt is not null ),
	CHECK( dureeLivre is not null ),
	CHECK( dureeCD is not null ),
	CHECK( dureeDVD is not null ),
	CHECK( dureeVideo is not null ),
	CHECK( emprunteurs is not null )
) NESTED TABLE emprunteurs STORE AS CATEMPR_Tab_Ref_emprunteur;
/


--------------------------------------------------------------------------------
-- emprunt														EMPRUNT
--
CREATE TABLE emprunt OF emprunt_t
(
	PRIMARY KEY (id),

	CHECK( dateEmprunt < dateRendu ),

	CHECK( dateEmprunt is not null ),
	CHECK( emprunteur is not null ),
	CHECK( exemplaire is not null )
);
/
