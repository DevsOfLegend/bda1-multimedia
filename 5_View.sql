--------------------------------------------------------------------------------
-- Destructeur de view :D
--
begin
  for r in (select view_name from user_views) loop
    execute immediate 'drop view ' || r.view_name;
  end loop;
end;
/

--------------------------------------------------------------------------------
-- Liste des documents
-- C'est un peu comme avoir la table document
--
CREATE VIEW listOfDocuments AS
	SELECT
		DISTINCT e.document.id AS id, e.document.titre AS titre, e.document.theme AS theme, e.document.nbrExpl AS nbrExpl,
		m.r AS motscles,
		a.r AS auteurs,
		e.document.editeur AS editeur,
		exs.r AS exemplaires,
		e.document.getType() AS typ,
		e.document.getDuree() AS duree,
		e.document.getNbrPages() AS nbrPages,
		e.document.getNbrSsTitre() AS nbrSsTitre,
		e.document.getFormate() AS formate
	FROM
		exemplaire e,
		TABLE(e.document.motscles) m,
		TABLE(e.document.auteurs) a,
		TABLE(e.document.exemplaires) exs
;
/
