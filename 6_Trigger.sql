-- Suppression de TOUT les trigger
begin
  for r in (select trigger_name from user_triggers WHERE trigger_name IN('TRG_CHECK_NBEMPRUNT','TRG_CHECK_DATES','TRG_CHECK_IS_LOANABLE','TRG_CHECK_EMPRUNT')) loop
    execute immediate 'drop trigger ' || r.trigger_name;
  end loop;
end;
/


--------------------------------------------------------------------------------
-- la date de naissance ne peut pas être supperieur à la date d'aujourd hui
CREATE OR REPLACE TRIGGER trg_check_dates
  BEFORE INSERT OR UPDATE ON auteur
  FOR EACH ROW
BEGIN
	IF( :new.dateN > SYSDATE ) THEN
		RAISE_APPLICATION_ERROR(-20404,'Invalide date: ' || to_char(:new.dateN, 'YYYY-MM-DD')	|| ' is in the future ! (Are you Marty McFly ?)' );
	END IF;
END;
/
-- TESTE DU TRIGGER
-- INSERT INTO auteur VALUES(42,'toto','0000','pre',TO_DATE('2012-12-24','YYYY-MM-DD'), ENS_REF_DOCUMENT());


--------------------------------------------------------------------------------
-- On ne peut pas emprunter un exemplaire en cours d'emprunt
--
-- PLS-00536: Navigation through REF variables is not supported in PL/SQL.
--
CREATE OR REPLACE TRIGGER trg_check_is_loanable
  BEFORE INSERT OR UPDATE ON emprunt
  FOR EACH ROW
DECLARE
	v_nbrXmpl NUMBER;
BEGIN
	-- On compte le nombre d'exemplaire qui ne sont pas rendu et qui on l'id: :new.id
	SELECT COUNT(em.id) INTO v_nbrXmpl
	FROM emprunt em
	WHERE
		em.exemplaire.id = :new.id
		AND
		em.dateRendu IS NULL;

	--IF v_nbrXmpl = 0 THEN-- Exemplaire pas emprunté
	--	-- Pass
	IF( v_nbrXmpl = 1 ) THEN-- Exmplaire déja emprunté
		-- Déjà en cours d'emprunt
		RAISE_APPLICATION_ERROR(-20404, 'Exemplaire déjà en cours d emprunt !');
	END IF;

	IF( v_nbrXmpl > 1 ) THEN
		RAISE_APPLICATION_ERROR(-20404, 'Données incohérantes !!!! v_nbrXmpl='||v_nbrXmpl);
	END IF;
END;
/


--------------------------------------------------------------------------------
-- Le nombre d'emprunt doit être inferieur à celui autorisé pour la catégorie de
-- l'emprunteur.
--
-- PLS-00536: Navigation through REF variables is not supported in PL/SQL.
--
CREATE OR REPLACE TRIGGER trg_check_NbEmprunt
  BEFORE INSERT ON emprunt
  FOR EACH ROW
DECLARE
	v_nbr INT;
BEGIN
	SELECT COUNT(em.id) INTO v_nbr
	FROM emprunt em
	WHERE
		em.emprunteur.id = :new.emprunteur.id
		AND
		em.dateRendu IS NULL;

	IF v_nbr >= :new.emprunteur.categorie.nbMaxEmprunt THEN
		RAISE_APPLICATION_ERROR(-20404, 'Vous ne pouvez pas emprunter plus de '||v_nbr||' documents simulatanément.');
	END IF;
END;
/


--------------------------------------------------------------------------------
-- On ne peut pas réumprunter de document si on est hors délai sur l'emprunt des
-- autres document
--
-- PLS-00536: Navigation through REF variables is not supported in PL/SQL.
--
CREATE OR REPLACE TRIGGER trg_check_Emprunt
  BEFORE INSERT ON emprunt
  FOR EACH ROW
DECLARE
	v_nbr INT;
BEGIN
	SELECT COUNT(em.id) INTO v_nbr
	FROM emprunt em, listOfDocuments lod
	WHERE
		em.emprunteur.id = :new.emprunteur.id
		AND
		em.dateRendu IS NULL
		AND
		lod.id = em.exemplaire.id
		AND
		(
			(
				lod.typ = 'LIVRE'
				AND
				em.dateEmprunt < SYSDATE-:new.emprunteur.categorie.dureeLivre
			)
			OR
			(
				lod.typ = 'CD'
				AND
				em.dateEmprunt < SYSDATE-:new.emprunteur.categorie.dureeCD
			)
			OR
			(
				lod.typ = 'DVD'
				AND
				em.dateEmprunt < SYSDATE-:new.emprunteur.categorie.dureeDVD
			)
			OR
			(
				lod.typ = 'VIDEO'
				AND
				em.dateEmprunt < SYSDATE-:new.emprunteur.categorie.dureeVideo
			)
		);

	IF v_nbr <> 0 THEN
		RAISE_APPLICATION_ERROR(-20404, 'Vous ne pouvez pas emprunter d autre document, car vous avez '||v_nbr||' documents en retard (non rendu).');
	END IF;
END;
/
