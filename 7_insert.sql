-- INSERTION LIVRE: alice au pays des merveilles
INSERT INTO mot_cle VALUES ( mot_cle_t(0, 'Walt Disney', ens_ref_document()) );
/
INSERT INTO mot_cle VALUES ( mot_cle_t(1, 'carte', ens_ref_document()) );
/
INSERT INTO mot_cle VALUES ( mot_cle_t(2, 'lapin', ens_ref_document()) );
/
INSERT INTO auteur VALUES ( auteur_t(0, 'Walt Disney', 0254145874, 'Disney', TO_DATE('1918-05-15','yyyy-mm-dd'), ens_ref_document()) );
/
INSERT INTO editeur VALUES ( editeur_t(0, 'Walt Disney', 0254145874, adresse_t(12, 'rue des ortilles', '', 87000, 'Limoges'), ens_ref_document()) );
/
INSERT INTO livre VALUES ( livre_t(0, 'alice au pays des merveilles', 'fantastique', 2, (SELECT ref(e) FROM editeur e WHERE e.id=0 ), ens_ref_auteur(), ens_ref_mot_cle(), ens_ref_exemplaire(), 24) );
/
INSERT INTO TABLE(SELECT l.motsCles FROM livre l WHERE l.id=0) VALUES ( (SELECT ref(m) FROM mot_cle m WHERE m.id=0) );
/
INSERT INTO TABLE(SELECT m.documents FROM mot_Cle m WHERE m.id=0) VALUES ( (SELECT ref(l) FROM livre l WHERE l.id=0) );
/
INSERT INTO TABLE(SELECT l.motsCles FROM livre l WHERE l.id=0) VALUES ( (SELECT ref(m) FROM mot_cle m WHERE m.id=1) );
/
INSERT INTO TABLE(SELECT m.documents FROM mot_Cle m WHERE m.id=1) VALUES ( (SELECT ref(l) FROM livre l WHERE l.id=0) );
/
INSERT INTO TABLE(SELECT l.motsCles FROM livre l WHERE l.id=0) VALUES ( (SELECT ref(m) FROM mot_cle m WHERE m.id=2) );
/
INSERT INTO TABLE(SELECT m.documents FROM mot_Cle m WHERE m.id=2) VALUES ( (SELECT ref(l) FROM livre l WHERE l.id=0) );
/
INSERT INTO TABLE(SELECT l.auteurs FROM livre l WHERE l.id=0) VALUES ( (SELECT ref(a) FROM auteur a WHERE a.id=0) );
/
INSERT INTO TABLE(SELECT a.documents FROM auteur a WHERE a.id=0) VALUES ( (SELECT ref(l) FROM livre l WHERE l.id=0) );
/
INSERT INTO exemplaire VALUES ( exemplaire_t(0, 1, (select ref(doc) FROM livre doc where doc.id=0), ens_ref_emprunt()) );
/
INSERT INTO TABLE(SELECT l.exemplaires FROM livre l WHERE l.id=0) VALUES((SELECT ref(ex) FROM exemplaire ex WHERE ex.id=0));
/
INSERT INTO exemplaire VALUES ( exemplaire_t(1, 1, (select ref(doc) FROM livre doc where doc.id=0), ens_ref_emprunt()) );
/
INSERT INTO TABLE(SELECT l.exemplaires FROM livre l WHERE l.id=0) VALUES((SELECT ref(ex) FROM exemplaire ex WHERE ex.id=1));
/
INSERT INTO TABLE(SELECT e.documents FROM editeur e WHERE e.id=0) VALUES((SELECT ref(doc) FROM livre doc WHERE doc.id=0));
/


-- INSERTION LIVRE: harry potter 7
INSERT INTO mot_cle VALUES ( mot_cle_t(3, 'sorcier', ens_ref_document()) );
/
INSERT INTO mot_cle VALUES ( mot_cle_t(4, 'geant', ens_ref_document()) );
/
INSERT INTO mot_cle VALUES ( mot_cle_t(5, 'celuidontonnedoitpasprononcerlenom', ens_ref_document()) );
/
INSERT INTO auteur VALUES ( auteur_t(1, 'J.K.ROWLING', 0254145874, 'Joanne', TO_DATE('1918-05-15','yyyy-mm-dd'), ens_ref_document()) );
/
INSERT INTO editeur VALUES ( editeur_t(1, 'Dunod', 0555352414, adresse_t(2, 'rue des jonquilles', '', 87000, 'Limoges'), ens_ref_document()) );
/
INSERT INTO livre VALUES ( livre_t(1, 'harry potter 7', 'fantastique', 4, (SELECT ref(e) FROM editeur e WHERE e.id=1 ), ens_ref_auteur(), ens_ref_mot_cle(), ens_ref_exemplaire(), 405) );
/
INSERT INTO TABLE(SELECT l.motsCles FROM livre l WHERE l.id=1) VALUES ( (SELECT ref(m) FROM mot_cle m WHERE m.id=3) );
/
INSERT INTO TABLE(SELECT m.documents FROM mot_Cle m WHERE m.id=3) VALUES ( (SELECT ref(l) FROM livre l WHERE l.id=1) );
/
INSERT INTO TABLE(SELECT l.motsCles FROM livre l WHERE l.id=1) VALUES ( (SELECT ref(m) FROM mot_cle m WHERE m.id=4) );
/
INSERT INTO TABLE(SELECT m.documents FROM mot_Cle m WHERE m.id=4) VALUES ( (SELECT ref(l) FROM livre l WHERE l.id=1) );
/
INSERT INTO TABLE(SELECT l.motsCles FROM livre l WHERE l.id=1) VALUES ( (SELECT ref(m) FROM mot_cle m WHERE m.id=5) );
/
INSERT INTO TABLE(SELECT m.documents FROM mot_Cle m WHERE m.id=5) VALUES ( (SELECT ref(l) FROM livre l WHERE l.id=1) );
/
INSERT INTO TABLE(SELECT l.auteurs FROM livre l WHERE l.id=1) VALUES ( (SELECT ref(a) FROM auteur a WHERE a.id=1) );
/
INSERT INTO TABLE(SELECT a.documents FROM auteur a WHERE a.id=1) VALUES ( (SELECT ref(l) FROM livre l WHERE l.id=1) );
/
INSERT INTO exemplaire VALUES ( exemplaire_t(2, 1, (select ref(doc) FROM livre doc where doc.id=1), ens_ref_emprunt()) );
/
INSERT INTO TABLE(SELECT l.exemplaires FROM livre l WHERE l.id=1) VALUES((SELECT ref(ex) FROM exemplaire ex WHERE ex.id=2));
/
INSERT INTO exemplaire VALUES ( exemplaire_t(3, 1, (select ref(doc) FROM livre doc where doc.id=1), ens_ref_emprunt()) );
/
INSERT INTO TABLE(SELECT l.exemplaires FROM livre l WHERE l.id=1) VALUES((SELECT ref(ex) FROM exemplaire ex WHERE ex.id=3));
/
INSERT INTO exemplaire VALUES ( exemplaire_t(4, 1, (select ref(doc) FROM livre doc where doc.id=1), ens_ref_emprunt()) );
/
INSERT INTO TABLE(SELECT l.exemplaires FROM livre l WHERE l.id=1) VALUES((SELECT ref(ex) FROM exemplaire ex WHERE ex.id=4));
/
INSERT INTO exemplaire VALUES ( exemplaire_t(5, 1, (select ref(doc) FROM livre doc where doc.id=1), ens_ref_emprunt()) );
/
INSERT INTO TABLE(SELECT l.exemplaires FROM livre l WHERE l.id=1) VALUES((SELECT ref(ex) FROM exemplaire ex WHERE ex.id=5));
/
INSERT INTO TABLE(SELECT e.documents FROM editeur e WHERE e.id=1) VALUES((SELECT ref(doc) FROM livre doc WHERE doc.id=1));
/


-- INSERTION LIVRE: le major parlait trop
INSERT INTO mot_cle VALUES ( mot_cle_t(6, 'mystere', ens_ref_document()) );
/
INSERT INTO mot_cle VALUES ( mot_cle_t(7, 'meurtre', ens_ref_document()) );
/
INSERT INTO auteur VALUES ( auteur_t(2, 'Chrystie', 0322514789, 'agatha', TO_DATE('1921-02-24','yyyy-mm-dd'), ens_ref_document()) );
/
INSERT INTO editeur VALUES ( editeur_t(2, 'Eyrolles', 0325211445, adresse_t(5, 'rue des lilas', '', 57100, 'thionville'), ens_ref_document()) );
/
INSERT INTO livre VALUES ( livre_t(2, 'le major parlait trop', 'policier', 1, (SELECT ref(e) FROM editeur e WHERE e.id=2 ), ens_ref_auteur(), ens_ref_mot_cle(), ens_ref_exemplaire(), 482) );
/
INSERT INTO TABLE(SELECT l.motsCles FROM livre l WHERE l.id=2) VALUES ( (SELECT ref(m) FROM mot_cle m WHERE m.id=6) );
/
INSERT INTO TABLE(SELECT m.documents FROM mot_Cle m WHERE m.id=6) VALUES ( (SELECT ref(l) FROM livre l WHERE l.id=2) );
/
INSERT INTO TABLE(SELECT l.motsCles FROM livre l WHERE l.id=2) VALUES ( (SELECT ref(m) FROM mot_cle m WHERE m.id=7) );
/
INSERT INTO TABLE(SELECT m.documents FROM mot_Cle m WHERE m.id=7) VALUES ( (SELECT ref(l) FROM livre l WHERE l.id=2) );
/
INSERT INTO TABLE(SELECT l.auteurs FROM livre l WHERE l.id=2) VALUES ( (SELECT ref(a) FROM auteur a WHERE a.id=2) );
/
INSERT INTO TABLE(SELECT a.documents FROM auteur a WHERE a.id=2) VALUES ( (SELECT ref(l) FROM livre l WHERE l.id=2) );
/
INSERT INTO exemplaire VALUES ( exemplaire_t(6, 1, (select ref(doc) FROM livre doc where doc.id=2), ens_ref_emprunt()) );
/
INSERT INTO TABLE(SELECT l.exemplaires FROM livre l WHERE l.id=2) VALUES((SELECT ref(ex) FROM exemplaire ex WHERE ex.id=6));
/
INSERT INTO TABLE(SELECT e.documents FROM editeur e WHERE e.id=2) VALUES((SELECT ref(doc) FROM livre doc WHERE doc.id=2));
/


-- INSERTION LIVRE: SQL pour les nuls
INSERT INTO mot_cle VALUES ( mot_cle_t(8, 'code', ens_ref_document()) );
/
INSERT INTO mot_cle VALUES ( mot_cle_t(9, 'tutoriel', ens_ref_document()) );
/
INSERT INTO auteur VALUES ( auteur_t(3, 'kenn', 0122547895, 'man', TO_DATE('1956-03-15','yyyy-mm-dd'), ens_ref_document()) );
/
INSERT INTO auteur VALUES ( auteur_t(4, 'john', 0233654896, 'mickael', TO_DATE('1978-07-15','yyyy-mm-dd'), ens_ref_document()) );
/
INSERT INTO auteur VALUES ( auteur_t(5, 'jordan', 0214587458, 'mickael', TO_DATE('1936-05-30','yyyy-mm-dd'), ens_ref_document()) );
/
INSERT INTO livre VALUES ( livre_t(3, 'SQL pour les nuls', 'informatique', 4, (SELECT ref(e) FROM editeur e WHERE e.id=2 ), ens_ref_auteur(), ens_ref_mot_cle(), ens_ref_exemplaire(), 432) );
/
INSERT INTO TABLE(SELECT l.motsCles FROM livre l WHERE l.id=3) VALUES ( (SELECT ref(m) FROM mot_cle m WHERE m.id=8) );
/
INSERT INTO TABLE(SELECT m.documents FROM mot_Cle m WHERE m.id=8) VALUES ( (SELECT ref(l) FROM livre l WHERE l.id=3) );
/
INSERT INTO TABLE(SELECT l.motsCles FROM livre l WHERE l.id=3) VALUES ( (SELECT ref(m) FROM mot_cle m WHERE m.id=9) );
/
INSERT INTO TABLE(SELECT m.documents FROM mot_Cle m WHERE m.id=9) VALUES ( (SELECT ref(l) FROM livre l WHERE l.id=3) );
/
INSERT INTO TABLE(SELECT l.auteurs FROM livre l WHERE l.id=3) VALUES ( (SELECT ref(a) FROM auteur a WHERE a.id=3) );
/
INSERT INTO TABLE(SELECT a.documents FROM auteur a WHERE a.id=3) VALUES ( (SELECT ref(l) FROM livre l WHERE l.id=3) );
/
INSERT INTO TABLE(SELECT l.auteurs FROM livre l WHERE l.id=3) VALUES ( (SELECT ref(a) FROM auteur a WHERE a.id=4) );
/
INSERT INTO TABLE(SELECT a.documents FROM auteur a WHERE a.id=4) VALUES ( (SELECT ref(l) FROM livre l WHERE l.id=3) );
/
INSERT INTO TABLE(SELECT l.auteurs FROM livre l WHERE l.id=3) VALUES ( (SELECT ref(a) FROM auteur a WHERE a.id=5) );
/
INSERT INTO TABLE(SELECT a.documents FROM auteur a WHERE a.id=5) VALUES ( (SELECT ref(l) FROM livre l WHERE l.id=3) );
/
INSERT INTO exemplaire VALUES ( exemplaire_t(7, 1, (select ref(doc) FROM livre doc where doc.id=3), ens_ref_emprunt()) );
/
INSERT INTO TABLE(SELECT l.exemplaires FROM livre l WHERE l.id=3) VALUES((SELECT ref(ex) FROM exemplaire ex WHERE ex.id=7));
/
INSERT INTO exemplaire VALUES ( exemplaire_t(8, 1, (select ref(doc) FROM livre doc where doc.id=3), ens_ref_emprunt()) );
/
INSERT INTO TABLE(SELECT l.exemplaires FROM livre l WHERE l.id=3) VALUES((SELECT ref(ex) FROM exemplaire ex WHERE ex.id=8));
/
INSERT INTO exemplaire VALUES ( exemplaire_t(9, 1, (select ref(doc) FROM livre doc where doc.id=3), ens_ref_emprunt()) );
/
INSERT INTO TABLE(SELECT l.exemplaires FROM livre l WHERE l.id=3) VALUES((SELECT ref(ex) FROM exemplaire ex WHERE ex.id=9));
/
INSERT INTO exemplaire VALUES ( exemplaire_t(10, 1, (select ref(doc) FROM livre doc where doc.id=3), ens_ref_emprunt()) );
/
INSERT INTO TABLE(SELECT l.exemplaires FROM livre l WHERE l.id=3) VALUES((SELECT ref(ex) FROM exemplaire ex WHERE ex.id=10));
/
INSERT INTO TABLE(SELECT e.documents FROM editeur e WHERE e.id=2) VALUES((SELECT ref(doc) FROM livre doc WHERE doc.id=3));
/


-- INSERTION LIVRE: rootkit, subverting the windows kernel
INSERT INTO mot_cle VALUES ( mot_cle_t(10, 'ring', ens_ref_document()) );
/
INSERT INTO auteur VALUES ( auteur_t(6, 'deck', 0456895847, 'Lea', TO_DATE('1925-12-15','yyyy-mm-dd'), ens_ref_document()) );
/
INSERT INTO auteur VALUES ( auteur_t(7, 'nuel', 0125458745, 'guillaume', TO_DATE('1918-05-15','yyyy-mm-dd'), ens_ref_document()) );
/
INSERT INTO auteur VALUES ( auteur_t(8, 'barry', 0522114478, 'thierno', TO_DATE('1918-05-15','yyyy-mm-dd'), ens_ref_document()) );
/
INSERT INTO livre VALUES ( livre_t(4, 'rootkit, subverting the windows kernel', 'informatique', 3, (SELECT ref(e) FROM editeur e WHERE e.id=2 ), ens_ref_auteur(), ens_ref_mot_cle(), ens_ref_exemplaire(), 467) );
/
INSERT INTO TABLE(SELECT l.motsCles FROM livre l WHERE l.id=4) VALUES ( (SELECT ref(m) FROM mot_cle m WHERE m.id=9) );
/
INSERT INTO TABLE(SELECT m.documents FROM mot_Cle m WHERE m.id=9) VALUES ( (SELECT ref(l) FROM livre l WHERE l.id=4) );
/
INSERT INTO TABLE(SELECT l.motsCles FROM livre l WHERE l.id=4) VALUES ( (SELECT ref(m) FROM mot_cle m WHERE m.id=8) );
/
INSERT INTO TABLE(SELECT m.documents FROM mot_Cle m WHERE m.id=8) VALUES ( (SELECT ref(l) FROM livre l WHERE l.id=4) );
/
INSERT INTO TABLE(SELECT l.motsCles FROM livre l WHERE l.id=4) VALUES ( (SELECT ref(m) FROM mot_cle m WHERE m.id=10) );
/
INSERT INTO TABLE(SELECT m.documents FROM mot_Cle m WHERE m.id=10) VALUES ( (SELECT ref(l) FROM livre l WHERE l.id=4) );
/
INSERT INTO TABLE(SELECT l.auteurs FROM livre l WHERE l.id=4) VALUES ( (SELECT ref(a) FROM auteur a WHERE a.id=6) );
/
INSERT INTO TABLE(SELECT a.documents FROM auteur a WHERE a.id=6) VALUES ( (SELECT ref(l) FROM livre l WHERE l.id=4) );
/
INSERT INTO TABLE(SELECT l.auteurs FROM livre l WHERE l.id=4) VALUES ( (SELECT ref(a) FROM auteur a WHERE a.id=7) );
/
INSERT INTO TABLE(SELECT a.documents FROM auteur a WHERE a.id=7) VALUES ( (SELECT ref(l) FROM livre l WHERE l.id=4) );
/
INSERT INTO TABLE(SELECT l.auteurs FROM livre l WHERE l.id=4) VALUES ( (SELECT ref(a) FROM auteur a WHERE a.id=8) );
/
INSERT INTO TABLE(SELECT a.documents FROM auteur a WHERE a.id=8) VALUES ( (SELECT ref(l) FROM livre l WHERE l.id=4) );
/
INSERT INTO exemplaire VALUES ( exemplaire_t(11, 1, (select ref(doc) FROM livre doc where doc.id=4), ens_ref_emprunt()) );
/
INSERT INTO TABLE(SELECT l.exemplaires FROM livre l WHERE l.id=4) VALUES((SELECT ref(ex) FROM exemplaire ex WHERE ex.id=11));
/
INSERT INTO exemplaire VALUES ( exemplaire_t(12, 1, (select ref(doc) FROM livre doc where doc.id=4), ens_ref_emprunt()) );
/
INSERT INTO TABLE(SELECT l.exemplaires FROM livre l WHERE l.id=4) VALUES((SELECT ref(ex) FROM exemplaire ex WHERE ex.id=12));
/
INSERT INTO exemplaire VALUES ( exemplaire_t(13, 1, (select ref(doc) FROM livre doc where doc.id=4), ens_ref_emprunt()) );
/
INSERT INTO TABLE(SELECT l.exemplaires FROM livre l WHERE l.id=4) VALUES((SELECT ref(ex) FROM exemplaire ex WHERE ex.id=13));
/
INSERT INTO TABLE(SELECT e.documents FROM editeur e WHERE e.id=2) VALUES((SELECT ref(doc) FROM livre doc WHERE doc.id=4));
/


-- INSERTION LIVRE: tuto cuisto
INSERT INTO mot_cle VALUES ( mot_cle_t(11, 'cuisine', ens_ref_document()) );
/
INSERT INTO auteur VALUES ( auteur_t(9, 'tavernier', 0215478549, 'sonny', TO_DATE('1946-08-25','yyyy-mm-dd'), ens_ref_document()) );
/
INSERT INTO auteur VALUES ( auteur_t(10, 'fleury', 0214587458, 'benoit', TO_DATE('1916-10-15','yyyy-mm-dd'), ens_ref_document()) );
/
INSERT INTO editeur VALUES ( editeur_t(3, 'Cesar', 0354874589, adresse_t(15, 'rue des tourbes', '', 57100, 'thionville'), ens_ref_document()) );
/
INSERT INTO livre VALUES ( livre_t(5, 'tuto cuisto', 'cuisine', 3, (SELECT ref(e) FROM editeur e WHERE e.id=3 ), ens_ref_auteur(), ens_ref_mot_cle(), ens_ref_exemplaire(), 117) );
/
INSERT INTO TABLE(SELECT l.motsCles FROM livre l WHERE l.id=5) VALUES ( (SELECT ref(m) FROM mot_cle m WHERE m.id=11) );
/
INSERT INTO TABLE(SELECT m.documents FROM mot_Cle m WHERE m.id=11) VALUES ( (SELECT ref(l) FROM livre l WHERE l.id=5) );
/
INSERT INTO TABLE(SELECT l.motsCles FROM livre l WHERE l.id=5) VALUES ( (SELECT ref(m) FROM mot_cle m WHERE m.id=9) );
/
INSERT INTO TABLE(SELECT m.documents FROM mot_Cle m WHERE m.id=9) VALUES ( (SELECT ref(l) FROM livre l WHERE l.id=5) );
/
INSERT INTO TABLE(SELECT l.auteurs FROM livre l WHERE l.id=5) VALUES ( (SELECT ref(a) FROM auteur a WHERE a.id=9) );
/
INSERT INTO TABLE(SELECT a.documents FROM auteur a WHERE a.id=9) VALUES ( (SELECT ref(l) FROM livre l WHERE l.id=5) );
/
INSERT INTO TABLE(SELECT l.auteurs FROM livre l WHERE l.id=5) VALUES ( (SELECT ref(a) FROM auteur a WHERE a.id=10) );
/
INSERT INTO TABLE(SELECT a.documents FROM auteur a WHERE a.id=10) VALUES ( (SELECT ref(l) FROM livre l WHERE l.id=5) );
/
INSERT INTO exemplaire VALUES ( exemplaire_t(14, 1, (select ref(doc) FROM livre doc where doc.id=5), ens_ref_emprunt()) );
/
INSERT INTO TABLE(SELECT l.exemplaires FROM livre l WHERE l.id=5) VALUES((SELECT ref(ex) FROM exemplaire ex WHERE ex.id=14));
/
INSERT INTO exemplaire VALUES ( exemplaire_t(15, 1, (select ref(doc) FROM livre doc where doc.id=5), ens_ref_emprunt()) );
/
INSERT INTO TABLE(SELECT l.exemplaires FROM livre l WHERE l.id=5) VALUES((SELECT ref(ex) FROM exemplaire ex WHERE ex.id=15));
/
INSERT INTO exemplaire VALUES ( exemplaire_t(16, 1, (select ref(doc) FROM livre doc where doc.id=5), ens_ref_emprunt()) );
/
INSERT INTO TABLE(SELECT l.exemplaires FROM livre l WHERE l.id=5) VALUES((SELECT ref(ex) FROM exemplaire ex WHERE ex.id=16));
/
INSERT INTO TABLE(SELECT e.documents FROM editeur e WHERE e.id=3) VALUES((SELECT ref(doc) FROM livre doc WHERE doc.id=5));
/


-- INSERTION LIVRE: tuto cuisto2
INSERT INTO editeur VALUES ( editeur_t(4, 'Socrate', 0566987486, adresse_t(7, 'rue des lilolas', '', 58100, 'thionville2'), ens_ref_document()) );
/
INSERT INTO livre VALUES ( livre_t(6, 'tuto cuisto2', 'cuisine', 2, (SELECT ref(e) FROM editeur e WHERE e.id=4 ), ens_ref_auteur(), ens_ref_mot_cle(), ens_ref_exemplaire(), 197) );
/
INSERT INTO TABLE(SELECT l.motsCles FROM livre l WHERE l.id=6) VALUES ( (SELECT ref(m) FROM mot_cle m WHERE m.id=9) );
/
INSERT INTO TABLE(SELECT m.documents FROM mot_Cle m WHERE m.id=9) VALUES ( (SELECT ref(l) FROM livre l WHERE l.id=6) );
/
INSERT INTO TABLE(SELECT l.auteurs FROM livre l WHERE l.id=6) VALUES ( (SELECT ref(a) FROM auteur a WHERE a.id=10) );
/
INSERT INTO TABLE(SELECT a.documents FROM auteur a WHERE a.id=10) VALUES ( (SELECT ref(l) FROM livre l WHERE l.id=6) );
/
INSERT INTO exemplaire VALUES ( exemplaire_t(17, 1, (select ref(doc) FROM livre doc where doc.id=6), ens_ref_emprunt()) );
/
INSERT INTO TABLE(SELECT l.exemplaires FROM livre l WHERE l.id=6) VALUES((SELECT ref(ex) FROM exemplaire ex WHERE ex.id=17));
/
INSERT INTO exemplaire VALUES ( exemplaire_t(18, 1, (select ref(doc) FROM livre doc where doc.id=6), ens_ref_emprunt()) );
/
INSERT INTO TABLE(SELECT l.exemplaires FROM livre l WHERE l.id=6) VALUES((SELECT ref(ex) FROM exemplaire ex WHERE ex.id=18));
/
INSERT INTO TABLE(SELECT e.documents FROM editeur e WHERE e.id=4) VALUES((SELECT ref(doc) FROM livre doc WHERE doc.id=6));
/


-- INSERTION LIVRE: Intelligence artificielle
INSERT INTO mot_cle VALUES ( mot_cle_t(12, 'robot', ens_ref_document()) );
/
INSERT INTO editeur VALUES ( editeur_t(5, 'Deck', 0325211445, adresse_t(14, 'rue des paquerette', '', 38440, 'Beauvoir de marc'), ens_ref_document()) );
/
INSERT INTO livre VALUES ( livre_t(7, 'Intelligence artificielle', 'mathematiques', 3, (SELECT ref(e) FROM editeur e WHERE e.id=5 ), ens_ref_auteur(), ens_ref_mot_cle(), ens_ref_exemplaire(), 429) );
/
INSERT INTO TABLE(SELECT l.motsCles FROM livre l WHERE l.id=7) VALUES ( (SELECT ref(m) FROM mot_cle m WHERE m.id=12) );
/
INSERT INTO TABLE(SELECT m.documents FROM mot_Cle m WHERE m.id=12) VALUES ( (SELECT ref(l) FROM livre l WHERE l.id=7) );
/
INSERT INTO TABLE(SELECT l.auteurs FROM livre l WHERE l.id=7) VALUES ( (SELECT ref(a) FROM auteur a WHERE a.id=3) );
/
INSERT INTO TABLE(SELECT a.documents FROM auteur a WHERE a.id=3) VALUES ( (SELECT ref(l) FROM livre l WHERE l.id=7) );
/
INSERT INTO exemplaire VALUES ( exemplaire_t(19, 1, (select ref(doc) FROM livre doc where doc.id=7), ens_ref_emprunt()) );
/
INSERT INTO TABLE(SELECT l.exemplaires FROM livre l WHERE l.id=7) VALUES((SELECT ref(ex) FROM exemplaire ex WHERE ex.id=19));
/
INSERT INTO exemplaire VALUES ( exemplaire_t(20, 1, (select ref(doc) FROM livre doc where doc.id=7), ens_ref_emprunt()) );
/
INSERT INTO TABLE(SELECT l.exemplaires FROM livre l WHERE l.id=7) VALUES((SELECT ref(ex) FROM exemplaire ex WHERE ex.id=20));
/
INSERT INTO exemplaire VALUES ( exemplaire_t(21, 1, (select ref(doc) FROM livre doc where doc.id=7), ens_ref_emprunt()) );
/
INSERT INTO TABLE(SELECT l.exemplaires FROM livre l WHERE l.id=7) VALUES((SELECT ref(ex) FROM exemplaire ex WHERE ex.id=21));
/
INSERT INTO TABLE(SELECT e.documents FROM editeur e WHERE e.id=5) VALUES((SELECT ref(doc) FROM livre doc WHERE doc.id=7));
/


-- INSERTION LIVRE: intelligence artificielle et algorithmique
INSERT INTO mot_cle VALUES ( mot_cle_t(13, 'intelligence', ens_ref_document()) );
/
INSERT INTO editeur VALUES ( editeur_t(6, 'Dupuis', 0355478951, adresse_t(5, 'rue des lipolas', '', 57140, 'noville'), ens_ref_document()) );
/
INSERT INTO livre VALUES ( livre_t(8, 'intelligence artificielle et algorithmique', 'mathematiques et informatiques', 1, (SELECT ref(e) FROM editeur e WHERE e.id=6 ), ens_ref_auteur(), ens_ref_mot_cle(), ens_ref_exemplaire(), 406) );
/
INSERT INTO TABLE(SELECT l.motsCles FROM livre l WHERE l.id=8) VALUES ( (SELECT ref(m) FROM mot_cle m WHERE m.id=13) );
/
INSERT INTO TABLE(SELECT m.documents FROM mot_Cle m WHERE m.id=13) VALUES ( (SELECT ref(l) FROM livre l WHERE l.id=8) );
/
INSERT INTO TABLE(SELECT l.motsCles FROM livre l WHERE l.id=8) VALUES ( (SELECT ref(m) FROM mot_cle m WHERE m.id=12) );
/
INSERT INTO TABLE(SELECT m.documents FROM mot_Cle m WHERE m.id=12) VALUES ( (SELECT ref(l) FROM livre l WHERE l.id=8) );
/
INSERT INTO TABLE(SELECT l.auteurs FROM livre l WHERE l.id=8) VALUES ( (SELECT ref(a) FROM auteur a WHERE a.id=10) );
/
INSERT INTO TABLE(SELECT a.documents FROM auteur a WHERE a.id=10) VALUES ( (SELECT ref(l) FROM livre l WHERE l.id=8) );
/
INSERT INTO exemplaire VALUES ( exemplaire_t(22, 1, (select ref(doc) FROM livre doc where doc.id=8), ens_ref_emprunt()) );
/
INSERT INTO TABLE(SELECT l.exemplaires FROM livre l WHERE l.id=8) VALUES((SELECT ref(ex) FROM exemplaire ex WHERE ex.id=22));
/
INSERT INTO TABLE(SELECT e.documents FROM editeur e WHERE e.id=6) VALUES((SELECT ref(doc) FROM livre doc WHERE doc.id=8));
/


-- INSERTION LIVRE: complexite et calculabitlite côte code
INSERT INTO mot_cle VALUES ( mot_cle_t(14, 'complexite', ens_ref_document()) );
/
INSERT INTO mot_cle VALUES ( mot_cle_t(15, 'calculabilite', ens_ref_document()) );
/
INSERT INTO mot_cle VALUES ( mot_cle_t(16, 'algorithme', ens_ref_document()) );
/
INSERT INTO editeur VALUES ( editeur_t(7, 'Nuel', 0325211445, adresse_t(6, 'rue des dionees attrapes mouches', 'appart 8', 67310, 'Wasselonne'), ens_ref_document()) );
/
INSERT INTO livre VALUES ( livre_t(9, 'complexite et calculabitlite côte code', 'mathematiques et informatiques', 1, (SELECT ref(e) FROM editeur e WHERE e.id=7 ), ens_ref_auteur(), ens_ref_mot_cle(), ens_ref_exemplaire(), 297) );
/
INSERT INTO TABLE(SELECT l.motsCles FROM livre l WHERE l.id=9) VALUES ( (SELECT ref(m) FROM mot_cle m WHERE m.id=14) );
/
INSERT INTO TABLE(SELECT m.documents FROM mot_Cle m WHERE m.id=14) VALUES ( (SELECT ref(l) FROM livre l WHERE l.id=9) );
/
INSERT INTO TABLE(SELECT l.motsCles FROM livre l WHERE l.id=9) VALUES ( (SELECT ref(m) FROM mot_cle m WHERE m.id=15) );
/
INSERT INTO TABLE(SELECT m.documents FROM mot_Cle m WHERE m.id=15) VALUES ( (SELECT ref(l) FROM livre l WHERE l.id=9) );
/
INSERT INTO TABLE(SELECT l.motsCles FROM livre l WHERE l.id=9) VALUES ( (SELECT ref(m) FROM mot_cle m WHERE m.id=16) );
/
INSERT INTO TABLE(SELECT m.documents FROM mot_Cle m WHERE m.id=16) VALUES ( (SELECT ref(l) FROM livre l WHERE l.id=9) );
/
INSERT INTO TABLE(SELECT l.auteurs FROM livre l WHERE l.id=9) VALUES ( (SELECT ref(a) FROM auteur a WHERE a.id=3) );
/
INSERT INTO TABLE(SELECT a.documents FROM auteur a WHERE a.id=3) VALUES ( (SELECT ref(l) FROM livre l WHERE l.id=9) );
/
INSERT INTO exemplaire VALUES ( exemplaire_t(23, 1, (select ref(doc) FROM livre doc where doc.id=9), ens_ref_emprunt()) );
/
INSERT INTO TABLE(SELECT l.exemplaires FROM livre l WHERE l.id=9) VALUES((SELECT ref(ex) FROM exemplaire ex WHERE ex.id=23));
/
INSERT INTO TABLE(SELECT e.documents FROM editeur e WHERE e.id=7) VALUES((SELECT ref(doc) FROM livre doc WHERE doc.id=9));
/


-- INSERTION CD: mozart
INSERT INTO mot_cle VALUES ( mot_cle_t(17, 'classique', ens_ref_document()) );
/
INSERT INTO cd VALUES ( cd_t(0, 'mozart', 'musique classique', 3, (SELECT ref(e) FROM editeur e WHERE e.id=5 ), ens_ref_auteur(), ens_ref_mot_cle(), ens_ref_exemplaire(), 61, 3) );
/
INSERT INTO TABLE(SELECT c.motsCles FROM cd c WHERE c.id=0) VALUES ( (SELECT ref(m) FROM mot_cle m WHERE m.id=17) );
/
INSERT INTO TABLE(SELECT m.documents FROM mot_Cle m WHERE m.id=17) VALUES ( (SELECT ref(c) FROM cd c WHERE c.id=0) );
/
INSERT INTO TABLE(SELECT c.auteurs FROM cd c WHERE c.id=0) VALUES ( (SELECT ref(a) FROM auteur a WHERE a.id=8) );
/
INSERT INTO TABLE(SELECT a.documents FROM auteur a WHERE a.id=8) VALUES ( (SELECT ref(c) FROM cd c WHERE c.id=0) );
/
INSERT INTO exemplaire VALUES ( exemplaire_t(24, 1, (select ref(doc) FROM cd doc where doc.id=0), ens_ref_emprunt()) );
/
INSERT INTO TABLE(SELECT c.exemplaires FROM cd c WHERE c.id=0) VALUES((SELECT ref(ex) FROM exemplaire ex WHERE ex.id=24));
/
INSERT INTO exemplaire VALUES ( exemplaire_t(25, 1, (select ref(doc) FROM cd doc where doc.id=0), ens_ref_emprunt()) );
/
INSERT INTO TABLE(SELECT c.exemplaires FROM cd c WHERE c.id=0) VALUES((SELECT ref(ex) FROM exemplaire ex WHERE ex.id=25));
/
INSERT INTO exemplaire VALUES ( exemplaire_t(26, 1, (select ref(doc) FROM cd doc where doc.id=0), ens_ref_emprunt()) );
/
INSERT INTO TABLE(SELECT c.exemplaires FROM cd c WHERE c.id=0) VALUES((SELECT ref(ex) FROM exemplaire ex WHERE ex.id=26));
/
INSERT INTO TABLE(SELECT e.documents FROM editeur e WHERE e.id=5) VALUES((SELECT ref(doc) FROM cd doc WHERE doc.id=0));
/


-- INSERTION CD: lindsey stirling
INSERT INTO mot_cle VALUES ( mot_cle_t(18, 'violon', ens_ref_document()) );
/
INSERT INTO cd VALUES ( cd_t(1, 'lindsey stirling', 'rock', 1, (SELECT ref(e) FROM editeur e WHERE e.id=2 ), ens_ref_auteur(), ens_ref_mot_cle(), ens_ref_exemplaire(), 256, 4) );
/
INSERT INTO TABLE(SELECT c.motsCles FROM cd c WHERE c.id=1) VALUES ( (SELECT ref(m) FROM mot_cle m WHERE m.id=18) );
/
INSERT INTO TABLE(SELECT m.documents FROM mot_Cle m WHERE m.id=18) VALUES ( (SELECT ref(c) FROM cd c WHERE c.id=1) );
/
INSERT INTO TABLE(SELECT c.auteurs FROM cd c WHERE c.id=1) VALUES ( (SELECT ref(a) FROM auteur a WHERE a.id=4) );
/
INSERT INTO TABLE(SELECT a.documents FROM auteur a WHERE a.id=4) VALUES ( (SELECT ref(c) FROM cd c WHERE c.id=1) );
/
INSERT INTO exemplaire VALUES ( exemplaire_t(27, 1, (select ref(doc) FROM cd doc where doc.id=1), ens_ref_emprunt()) );
/
INSERT INTO TABLE(SELECT c.exemplaires FROM cd c WHERE c.id=1) VALUES((SELECT ref(ex) FROM exemplaire ex WHERE ex.id=27));
/
INSERT INTO TABLE(SELECT e.documents FROM editeur e WHERE e.id=2) VALUES((SELECT ref(doc) FROM cd doc WHERE doc.id=1));
/


-- INSERTION CD: metallica
INSERT INTO mot_cle VALUES ( mot_cle_t(19, 'rock', ens_ref_document()) );
/
INSERT INTO cd VALUES ( cd_t(2, 'metallica', 'rock', 3, (SELECT ref(e) FROM editeur e WHERE e.id=1 ), ens_ref_auteur(), ens_ref_mot_cle(), ens_ref_exemplaire(), 22, 3) );
/
INSERT INTO TABLE(SELECT c.motsCles FROM cd c WHERE c.id=2) VALUES ( (SELECT ref(m) FROM mot_cle m WHERE m.id=19) );
/
INSERT INTO TABLE(SELECT m.documents FROM mot_Cle m WHERE m.id=19) VALUES ( (SELECT ref(c) FROM cd c WHERE c.id=2) );
/
INSERT INTO TABLE(SELECT c.auteurs FROM cd c WHERE c.id=2) VALUES ( (SELECT ref(a) FROM auteur a WHERE a.id=8) );
/
INSERT INTO TABLE(SELECT a.documents FROM auteur a WHERE a.id=8) VALUES ( (SELECT ref(c) FROM cd c WHERE c.id=2) );
/
INSERT INTO exemplaire VALUES ( exemplaire_t(28, 1, (select ref(doc) FROM cd doc where doc.id=2), ens_ref_emprunt()) );
/
INSERT INTO TABLE(SELECT c.exemplaires FROM cd c WHERE c.id=2) VALUES((SELECT ref(ex) FROM exemplaire ex WHERE ex.id=28));
/
INSERT INTO exemplaire VALUES ( exemplaire_t(29, 1, (select ref(doc) FROM cd doc where doc.id=2), ens_ref_emprunt()) );
/
INSERT INTO TABLE(SELECT c.exemplaires FROM cd c WHERE c.id=2) VALUES((SELECT ref(ex) FROM exemplaire ex WHERE ex.id=29));
/
INSERT INTO exemplaire VALUES ( exemplaire_t(30, 1, (select ref(doc) FROM cd doc where doc.id=2), ens_ref_emprunt()) );
/
INSERT INTO TABLE(SELECT c.exemplaires FROM cd c WHERE c.id=2) VALUES((SELECT ref(ex) FROM exemplaire ex WHERE ex.id=30));
/
INSERT INTO TABLE(SELECT e.documents FROM editeur e WHERE e.id=1) VALUES((SELECT ref(doc) FROM cd doc WHERE doc.id=2));
/


-- INSERTION CD: epica
INSERT INTO mot_cle VALUES ( mot_cle_t(20, 'symphonie', ens_ref_document()) );
/
INSERT INTO mot_cle VALUES ( mot_cle_t(21, 'live', ens_ref_document()) );
/
INSERT INTO cd VALUES ( cd_t(3, 'epica', 'metal synphonique', 1, (SELECT ref(e) FROM editeur e WHERE e.id=2 ), ens_ref_auteur(), ens_ref_mot_cle(), ens_ref_exemplaire(), 110, 1) );
/
INSERT INTO TABLE(SELECT c.motsCles FROM cd c WHERE c.id=3) VALUES ( (SELECT ref(m) FROM mot_cle m WHERE m.id=20) );
/
INSERT INTO TABLE(SELECT m.documents FROM mot_Cle m WHERE m.id=20) VALUES ( (SELECT ref(c) FROM cd c WHERE c.id=3) );
/
INSERT INTO TABLE(SELECT c.motsCles FROM cd c WHERE c.id=3) VALUES ( (SELECT ref(m) FROM mot_cle m WHERE m.id=21) );
/
INSERT INTO TABLE(SELECT m.documents FROM mot_Cle m WHERE m.id=21) VALUES ( (SELECT ref(c) FROM cd c WHERE c.id=3) );
/
INSERT INTO TABLE(SELECT c.auteurs FROM cd c WHERE c.id=3) VALUES ( (SELECT ref(a) FROM auteur a WHERE a.id=9) );
/
INSERT INTO TABLE(SELECT a.documents FROM auteur a WHERE a.id=9) VALUES ( (SELECT ref(c) FROM cd c WHERE c.id=3) );
/
INSERT INTO exemplaire VALUES ( exemplaire_t(31, 1, (select ref(doc) FROM cd doc where doc.id=3), ens_ref_emprunt()) );
/
INSERT INTO TABLE(SELECT c.exemplaires FROM cd c WHERE c.id=3) VALUES((SELECT ref(ex) FROM exemplaire ex WHERE ex.id=31));
/
INSERT INTO TABLE(SELECT e.documents FROM editeur e WHERE e.id=2) VALUES((SELECT ref(doc) FROM cd doc WHERE doc.id=3));
/


-- INSERTION DVD: john carter
INSERT INTO mot_cle VALUES ( mot_cle_t(22, 'mars', ens_ref_document()) );
/
INSERT INTO dvd VALUES ( media_t(0, 'john carter', 'fantastique', 1, (SELECT ref(e) FROM editeur e WHERE e.id=0 ), ens_ref_auteur(), ens_ref_mot_cle(), ens_ref_exemplaire(), 485) );
/
INSERT INTO TABLE(SELECT c.motsCles FROM dvd c WHERE c.id=0) VALUES ( (SELECT ref(m) FROM mot_cle m WHERE m.id=0 ));
/
INSERT INTO TABLE(SELECT m.documents FROM mot_Cle m WHERE m.id=0) VALUES ( (SELECT ref(c) FROM dvd c WHERE c.id=0 ));
/
INSERT INTO TABLE(SELECT c.motsCles FROM dvd c WHERE c.id=0) VALUES ( (SELECT ref(m) FROM mot_cle m WHERE m.id=22 ));
/
INSERT INTO TABLE(SELECT m.documents FROM mot_Cle m WHERE m.id=22) VALUES ( (SELECT ref(c) FROM dvd c WHERE c.id=0 ));
/
INSERT INTO TABLE(SELECT c.auteurs FROM dvd c WHERE c.id=0) VALUES ( (SELECT ref(a) FROM auteur a WHERE a.id=0 ));
/
INSERT INTO TABLE(SELECT a.documents FROM auteur a WHERE a.id=0) VALUES ( (SELECT ref(c) FROM dvd c WHERE c.id=0 ));
/
INSERT INTO exemplaire VALUES ( exemplaire_t(32, 1, (select ref(doc) FROM dvd doc where doc.id=0), ens_ref_emprunt()) );
/
INSERT INTO TABLE(SELECT c.exemplaires FROM dvd c WHERE c.id=0) VALUES((SELECT ref(ex) FROM exemplaire ex WHERE ex.id=32));
/
INSERT INTO TABLE(SELECT e.documents FROM editeur e WHERE e.id=0) VALUES((SELECT ref(doc) FROM dvd doc WHERE doc.id=0));
/


-- INSERTION DVD: Apprendre à coder Jquery
INSERT INTO dvd VALUES ( media_t(1, 'Apprendre à coder Jquery', 'informatique', 1, (SELECT ref(e) FROM editeur e WHERE e.id=2 ), ens_ref_auteur(), ens_ref_mot_cle(), ens_ref_exemplaire(), 134) );
/
INSERT INTO TABLE(SELECT c.motsCles FROM dvd c WHERE c.id=1) VALUES ( (SELECT ref(m) FROM mot_cle m WHERE m.id=8 ));
/
INSERT INTO TABLE(SELECT m.documents FROM mot_Cle m WHERE m.id=8) VALUES ( (SELECT ref(c) FROM dvd c WHERE c.id=1 ));
/
INSERT INTO TABLE(SELECT c.auteurs FROM dvd c WHERE c.id=1) VALUES ( (SELECT ref(a) FROM auteur a WHERE a.id=9 ));
/
INSERT INTO TABLE(SELECT a.documents FROM auteur a WHERE a.id=9) VALUES ( (SELECT ref(c) FROM dvd c WHERE c.id=1 ));
/
INSERT INTO exemplaire VALUES ( exemplaire_t(33, 1, (select ref(doc) FROM dvd doc where doc.id=1), ens_ref_emprunt()) );
/
INSERT INTO TABLE(SELECT c.exemplaires FROM dvd c WHERE c.id=1) VALUES((SELECT ref(ex) FROM exemplaire ex WHERE ex.id=33));
/
INSERT INTO TABLE(SELECT e.documents FROM editeur e WHERE e.id=2) VALUES((SELECT ref(doc) FROM dvd doc WHERE doc.id=1));
/


-- INSERTION DVD: C est pas sorcier
INSERT INTO mot_cle VALUES ( mot_cle_t(23, 'science', ens_ref_document()) );
/
INSERT INTO mot_cle VALUES ( mot_cle_t(24, 'camion', ens_ref_document()) );
/
INSERT INTO dvd VALUES ( media_t(2, 'C est pas sorcier', 'scientifique et informatique', 1, (SELECT ref(e) FROM editeur e WHERE e.id=5 ), ens_ref_auteur(), ens_ref_mot_cle(), ens_ref_exemplaire(), 283) );
/
INSERT INTO TABLE(SELECT c.motsCles FROM dvd c WHERE c.id=2) VALUES ( (SELECT ref(m) FROM mot_cle m WHERE m.id=23 ));
/
INSERT INTO TABLE(SELECT m.documents FROM mot_Cle m WHERE m.id=23) VALUES ( (SELECT ref(c) FROM dvd c WHERE c.id=2 ));
/
INSERT INTO TABLE(SELECT c.motsCles FROM dvd c WHERE c.id=2) VALUES ( (SELECT ref(m) FROM mot_cle m WHERE m.id=24 ));
/
INSERT INTO TABLE(SELECT m.documents FROM mot_Cle m WHERE m.id=24) VALUES ( (SELECT ref(c) FROM dvd c WHERE c.id=2 ));
/
INSERT INTO TABLE(SELECT c.auteurs FROM dvd c WHERE c.id=2) VALUES ( (SELECT ref(a) FROM auteur a WHERE a.id=8 ));
/
INSERT INTO TABLE(SELECT a.documents FROM auteur a WHERE a.id=8) VALUES ( (SELECT ref(c) FROM dvd c WHERE c.id=2 ));
/
INSERT INTO exemplaire VALUES ( exemplaire_t(34, 1, (select ref(doc) FROM dvd doc where doc.id=2), ens_ref_emprunt()) );
/
INSERT INTO TABLE(SELECT c.exemplaires FROM dvd c WHERE c.id=2) VALUES((SELECT ref(ex) FROM exemplaire ex WHERE ex.id=34));
/
INSERT INTO TABLE(SELECT e.documents FROM editeur e WHERE e.id=5) VALUES((SELECT ref(doc) FROM dvd doc WHERE doc.id=2));
/


-- INSERTION DVD: Chérie j ai rétrécis les gosses
INSERT INTO mot_cle VALUES ( mot_cle_t(25, 'minuscule', ens_ref_document()) );
/
INSERT INTO dvd VALUES ( media_t(3, 'Chérie j ai rétrécis les gosses', 'science fiction', 1, (SELECT ref(e) FROM editeur e WHERE e.id=5 ), ens_ref_auteur(), ens_ref_mot_cle(), ens_ref_exemplaire(), 151) );
/
INSERT INTO TABLE(SELECT c.motsCles FROM dvd c WHERE c.id=3) VALUES ( (SELECT ref(m) FROM mot_cle m WHERE m.id=25 ));
/
INSERT INTO TABLE(SELECT m.documents FROM mot_Cle m WHERE m.id=25) VALUES ( (SELECT ref(c) FROM dvd c WHERE c.id=3 ));
/
INSERT INTO TABLE(SELECT c.auteurs FROM dvd c WHERE c.id=3) VALUES ( (SELECT ref(a) FROM auteur a WHERE a.id=8 ));
/
INSERT INTO TABLE(SELECT a.documents FROM auteur a WHERE a.id=8) VALUES ( (SELECT ref(c) FROM dvd c WHERE c.id=3 ));
/
INSERT INTO exemplaire VALUES ( exemplaire_t(35, 1, (select ref(doc) FROM dvd doc where doc.id=3), ens_ref_emprunt()) );
/
INSERT INTO TABLE(SELECT c.exemplaires FROM dvd c WHERE c.id=3) VALUES((SELECT ref(ex) FROM exemplaire ex WHERE ex.id=35));
/
INSERT INTO TABLE(SELECT e.documents FROM editeur e WHERE e.id=5) VALUES((SELECT ref(doc) FROM dvd doc WHERE doc.id=3));
/


-- INSERTION VIDEO: Tarte et gateaux
INSERT INTO mot_cle VALUES ( mot_cle_t(26, 'plat', ens_ref_document()) );
/
INSERT INTO video VALUES ( video_t(0, 'Tarte et gateaux', 'tutoriel de cuisine', 2, (SELECT ref(e) FROM editeur e WHERE e.id=2 ), ens_ref_auteur(), ens_ref_mot_cle(), ens_ref_exemplaire(), 330, 'AVI') );
/
INSERT INTO TABLE(SELECT c.motsCles FROM video c WHERE c.id=0) VALUES ( (SELECT ref(m) FROM mot_cle m WHERE m.id=26) );
/
INSERT INTO TABLE(SELECT m.documents FROM mot_Cle m WHERE m.id=26) VALUES ( (SELECT ref(c) FROM video c WHERE c.id=0) );
/
INSERT INTO TABLE(SELECT c.auteurs FROM video c WHERE c.id=0) VALUES ( (SELECT ref(a) FROM auteur a WHERE a.id=4) );
/
INSERT INTO TABLE(SELECT a.documents FROM auteur a WHERE a.id=4) VALUES ( (SELECT ref(c) FROM video c WHERE c.id=0) );
/
INSERT INTO exemplaire VALUES ( exemplaire_t(36, 1, (select ref(doc) FROM video doc where doc.id=0), ens_ref_emprunt()) );
/
INSERT INTO TABLE(SELECT c.exemplaires FROM video c WHERE c.id=0) VALUES((SELECT ref(ex) FROM exemplaire ex WHERE ex.id=36));
/
INSERT INTO exemplaire VALUES ( exemplaire_t(37, 1, (select ref(doc) FROM video doc where doc.id=0), ens_ref_emprunt()) );
/
INSERT INTO TABLE(SELECT c.exemplaires FROM video c WHERE c.id=0) VALUES((SELECT ref(ex) FROM exemplaire ex WHERE ex.id=37));
/
INSERT INTO TABLE(SELECT e.documents FROM editeur e WHERE e.id=2) VALUES((SELECT ref(doc) FROM video doc WHERE doc.id=0));
/


-- INSERTION VIDEO: hacker et Cie
INSERT INTO mot_cle VALUES ( mot_cle_t(27, 'hook', ens_ref_document()) );
/
INSERT INTO video VALUES ( video_t(1, 'hacker et Cie', 'korben info : securite informatique', 2, (SELECT ref(e) FROM editeur e WHERE e.id=2 ), ens_ref_auteur(), ens_ref_mot_cle(), ens_ref_exemplaire(), 260, 'AVI') );
/
INSERT INTO TABLE(SELECT c.motsCles FROM video c WHERE c.id=1) VALUES ( (SELECT ref(m) FROM mot_cle m WHERE m.id=27) );
/
INSERT INTO TABLE(SELECT m.documents FROM mot_Cle m WHERE m.id=27) VALUES ( (SELECT ref(c) FROM video c WHERE c.id=1) );
/
INSERT INTO TABLE(SELECT c.auteurs FROM video c WHERE c.id=1) VALUES ( (SELECT ref(a) FROM auteur a WHERE a.id=4) );
/
INSERT INTO TABLE(SELECT a.documents FROM auteur a WHERE a.id=4) VALUES ( (SELECT ref(c) FROM video c WHERE c.id=1) );
/
INSERT INTO exemplaire VALUES ( exemplaire_t(38, 1, (select ref(doc) FROM video doc where doc.id=1), ens_ref_emprunt()) );
/
INSERT INTO TABLE(SELECT c.exemplaires FROM video c WHERE c.id=1) VALUES((SELECT ref(ex) FROM exemplaire ex WHERE ex.id=38));
/
INSERT INTO exemplaire VALUES ( exemplaire_t(39, 1, (select ref(doc) FROM video doc where doc.id=1), ens_ref_emprunt()) );
/
INSERT INTO TABLE(SELECT c.exemplaires FROM video c WHERE c.id=1) VALUES((SELECT ref(ex) FROM exemplaire ex WHERE ex.id=39));
/
INSERT INTO TABLE(SELECT e.documents FROM editeur e WHERE e.id=2) VALUES((SELECT ref(doc) FROM video doc WHERE doc.id=1));
/


-- INSERTION VIDEO: La sigification scientiques de la forme des nids d abeilles
INSERT INTO mot_cle VALUES ( mot_cle_t(28, 'rond', ens_ref_document()) );
/
INSERT INTO mot_cle VALUES ( mot_cle_t(29, 'regle', ens_ref_document()) );
/
INSERT INTO mot_cle VALUES ( mot_cle_t(30, 'stylo', ens_ref_document()) );
/
INSERT INTO video VALUES ( video_t(2, 'La sigification scientiques de la forme des nids d abeilles', 'scientifique et mathematiques', 1, (SELECT ref(e) FROM editeur e WHERE e.id=2 ), ens_ref_auteur(), ens_ref_mot_cle(), ens_ref_exemplaire(), 28, 'AVI') );
/
INSERT INTO TABLE(SELECT c.motsCles FROM video c WHERE c.id=2) VALUES ( (SELECT ref(m) FROM mot_cle m WHERE m.id=28) );
/
INSERT INTO TABLE(SELECT m.documents FROM mot_Cle m WHERE m.id=28) VALUES ( (SELECT ref(c) FROM video c WHERE c.id=2) );
/
INSERT INTO TABLE(SELECT c.motsCles FROM video c WHERE c.id=2) VALUES ( (SELECT ref(m) FROM mot_cle m WHERE m.id=29) );
/
INSERT INTO TABLE(SELECT m.documents FROM mot_Cle m WHERE m.id=29) VALUES ( (SELECT ref(c) FROM video c WHERE c.id=2) );
/
INSERT INTO TABLE(SELECT c.motsCles FROM video c WHERE c.id=2) VALUES ( (SELECT ref(m) FROM mot_cle m WHERE m.id=30) );
/
INSERT INTO TABLE(SELECT m.documents FROM mot_Cle m WHERE m.id=30) VALUES ( (SELECT ref(c) FROM video c WHERE c.id=2) );
/
INSERT INTO TABLE(SELECT c.auteurs FROM video c WHERE c.id=2) VALUES ( (SELECT ref(a) FROM auteur a WHERE a.id=4) );
/
INSERT INTO TABLE(SELECT a.documents FROM auteur a WHERE a.id=4) VALUES ( (SELECT ref(c) FROM video c WHERE c.id=2) );
/
INSERT INTO exemplaire VALUES ( exemplaire_t(40, 1, (select ref(doc) FROM video doc where doc.id=2), ens_ref_emprunt()) );
/
INSERT INTO TABLE(SELECT c.exemplaires FROM video c WHERE c.id=2) VALUES((SELECT ref(ex) FROM exemplaire ex WHERE ex.id=40));
/
INSERT INTO TABLE(SELECT e.documents FROM editeur e WHERE e.id=2) VALUES((SELECT ref(doc) FROM video doc WHERE doc.id=2));
/


-- INSERTION CATEGORIE: personnel
INSERT INTO categorie_empr VALUES (categorie_empr_t(0, 'personnel', 10, 30, 21, 21, 21, ens_ref_emprunteur()));
/


-- INSERTION CATEGORIE: professionnel
INSERT INTO categorie_empr VALUES (categorie_empr_t(1, 'professionnel', 5, 30, 14, 14, 14, ens_ref_emprunteur()));
/


-- INSERTION CATEGORIE: public
INSERT INTO categorie_empr VALUES (categorie_empr_t(2, 'public', 3, 15, 7, 5, 7, ens_ref_emprunteur()));
/


-- INSERTION EMPRUNTEUR: dupont
INSERT INTO emprunteur VALUES ( emprunteur_t(0, 'dupont', 0524157825, 'titi', adresse_t(12, 'rue des ortilles', '', 87000, 'Limoges'), (SELECT ref(c) FROM categorie_empr c WHERE c.id=1), ens_ref_emprunt()) );
/
INSERT INTO TABLE( SELECT c.emprunteurs FROM categorie_empr c WHERE c.id=1) VALUES((SELECT ref(e) FROM emprunteur e WHERE e.id=0));

-- INSERTION EMPRUNTEUR: heury
INSERT INTO emprunteur VALUES ( emprunteur_t(1, 'heury', 0214587459, 'tat', adresse_t(12, 'rue des ortilles', '', 87000, 'Limoges'), (SELECT ref(c) FROM categorie_empr c WHERE c.id=1), ens_ref_emprunt()) );
/
INSERT INTO TABLE( SELECT c.emprunteurs FROM categorie_empr c WHERE c.id=1) VALUES((SELECT ref(e) FROM emprunteur e WHERE e.id=1));

-- INSERTION EMPRUNTEUR: hatchoum
INSERT INTO emprunteur VALUES ( emprunteur_t(2, 'hatchoum', 0356847512, 'ouioui', adresse_t(12, 'rue des ortilles', '', 87000, 'Limoges'), (SELECT ref(c) FROM categorie_empr c WHERE c.id=2), ens_ref_emprunt()) );
/
INSERT INTO TABLE( SELECT c.emprunteurs FROM categorie_empr c WHERE c.id=2) VALUES((SELECT ref(e) FROM emprunteur e WHERE e.id=2));

-- INSERTION EMPRUNTEUR: niourem
INSERT INTO emprunteur VALUES ( emprunteur_t(3, 'niourem', 0356847512, 'ahahaha', adresse_t(2, 'rue des mistrals gagnants', '', 87000, 'Limoges'), (SELECT ref(c) FROM categorie_empr c WHERE c.id=2), ens_ref_emprunt()) );
/
INSERT INTO TABLE( SELECT c.emprunteurs FROM categorie_empr c WHERE c.id=2) VALUES((SELECT ref(e) FROM emprunteur e WHERE e.id=3));

-- INSERTION EMPRUNTEUR: thieury
INSERT INTO emprunteur VALUES ( emprunteur_t(4, 'thieury', 0214587458, 'toto', adresse_t(8, 'rue des parapluies', '', 87000, 'Limoges'), (SELECT ref(c) FROM categorie_empr c WHERE c.id=2), ens_ref_emprunt()) );
/
INSERT INTO TABLE( SELECT c.emprunteurs FROM categorie_empr c WHERE c.id=2) VALUES((SELECT ref(e) FROM emprunteur e WHERE e.id=4));

-- INSERTION EMPRUNTEUR: jeury
INSERT INTO emprunteur VALUES ( emprunteur_t(5, 'jeury', 0214587451, 'ami', adresse_t(6, 'rue des grenouilles', '', 87000, 'Limoges'), (SELECT ref(c) FROM categorie_empr c WHERE c.id=2), ens_ref_emprunt()) );
/
INSERT INTO TABLE( SELECT c.emprunteurs FROM categorie_empr c WHERE c.id=2) VALUES((SELECT ref(e) FROM emprunteur e WHERE e.id=5));

-- INSERTION EMPRUNTEUR: mouru
INSERT INTO emprunteur VALUES ( emprunteur_t(6, 'mouru', 0214587459, 'mamo', adresse_t(8, 'rue des serpent', '', 87000, 'Limoges'), (SELECT ref(c) FROM categorie_empr c WHERE c.id=1), ens_ref_emprunt()) );
/
INSERT INTO TABLE( SELECT c.emprunteurs FROM categorie_empr c WHERE c.id=1) VALUES((SELECT ref(e) FROM emprunteur e WHERE e.id=6));

-- INSERTION EMPRUNTEUR: nemnem
INSERT INTO emprunteur VALUES ( emprunteur_t(7, 'nemnem', 0356847512, 'teletobise', adresse_t(7, 'rue des nexus castrus', '', 87000, 'Limoges'), (SELECT ref(c) FROM categorie_empr c WHERE c.id=1), ens_ref_emprunt()) );
/
INSERT INTO TABLE( SELECT c.emprunteurs FROM categorie_empr c WHERE c.id=1) VALUES((SELECT ref(e) FROM emprunteur e WHERE e.id=7));

-- INSERTION EMPRUNTEUR: miamiam
INSERT INTO emprunteur VALUES ( emprunteur_t(8, 'miamiam', 0356847512, 'pikatchou', adresse_t(16, 'rue des tiranosaure', '', 87000, 'Limoges'), (SELECT ref(c) FROM categorie_empr c WHERE c.id=1), ens_ref_emprunt()) );
/
INSERT INTO TABLE( SELECT c.emprunteurs FROM categorie_empr c WHERE c.id=1) VALUES((SELECT ref(e) FROM emprunteur e WHERE e.id=8));

-- INSERTION EMPRUNTEUR: bijoux
INSERT INTO emprunteur VALUES ( emprunteur_t(9, 'bijoux', 0356847512, 'pc', adresse_t(16, 'rue des tiranosaure', '', 87000, 'Limoges'), (SELECT ref(c) FROM categorie_empr c WHERE c.id=2), ens_ref_emprunt()) );
/
INSERT INTO TABLE( SELECT c.emprunteurs FROM categorie_empr c WHERE c.id=2) VALUES((SELECT ref(e) FROM emprunteur e WHERE e.id=9));

-- INSERTION EMPRUNTEUR: marre
INSERT INTO emprunteur VALUES ( emprunteur_t(10, 'marre', 0214587451, 'tita', adresse_t(3, 'rue des coq', '', 87000, 'Limoges'), (SELECT ref(c) FROM categorie_empr c WHERE c.id=2), ens_ref_emprunt()) );
/
INSERT INTO TABLE( SELECT c.emprunteurs FROM categorie_empr c WHERE c.id=2) VALUES((SELECT ref(e) FROM emprunteur e WHERE e.id=10));

-- INSERTION EMPRUNTEUR: flemme
INSERT INTO emprunteur VALUES ( emprunteur_t(11, 'flemme', 0214587458, 'tito', adresse_t(49, 'rue des galets', 'appartement 10', 87000, 'Limoges'), (SELECT ref(c) FROM categorie_empr c WHERE c.id=2), ens_ref_emprunt()) );
/
INSERT INTO TABLE( SELECT c.emprunteurs FROM categorie_empr c WHERE c.id=2) VALUES((SELECT ref(e) FROM emprunteur e WHERE e.id=11));

-- INSERTION EMPRUNTEUR: petage de cable
INSERT INTO emprunteur VALUES ( emprunteur_t(12, 'petage de cable', 0214587458, 'tramouet', adresse_t(2, 'rue des requins', '', 87000, 'Limoges'), (SELECT ref(c) FROM categorie_empr c WHERE c.id=2), ens_ref_emprunt()) );
/
INSERT INTO TABLE( SELECT c.emprunteurs FROM categorie_empr c WHERE c.id=2) VALUES((SELECT ref(e) FROM emprunteur e WHERE e.id=12));

-- INSERTION EMPRUNTEUR: guichour
INSERT INTO emprunteur VALUES ( emprunteur_t(13, 'guichour', 0145874589, 'mouchoir', adresse_t(7, 'rue des cepes', '', 87000, 'Limoges'), (SELECT ref(c) FROM categorie_empr c WHERE c.id=2), ens_ref_emprunt()) );
/
INSERT INTO TABLE( SELECT c.emprunteurs FROM categorie_empr c WHERE c.id=2) VALUES((SELECT ref(e) FROM emprunteur e WHERE e.id=13));

-- INSERTION EMPRUNTEUR: bijour
INSERT INTO emprunteur VALUES ( emprunteur_t(14, 'bijour', 0356847512, 'ram', adresse_t(59, 'rue des bolets', '', 87000, 'Limoges'), (SELECT ref(c) FROM categorie_empr c WHERE c.id=1), ens_ref_emprunt()) );
/
INSERT INTO TABLE( SELECT c.emprunteurs FROM categorie_empr c WHERE c.id=1) VALUES((SELECT ref(e) FROM emprunteur e WHERE e.id=14));

-- INSERTION EMPRUNT: le major parlait trop:0
INSERT INTO emprunt VALUES ( emprunt_t(0, TO_DATE('20011203','YYYYMMDD'), TO_DATE('20011210','YYYYMMDD'), (SELECT ref(e) FROM emprunteur e WHERE e.id=0), (SELECT ref(e) FROM exemplaire e WHERE e.id=6) ) );
/
INSERT INTO TABLE( SELECT c.emprunts FROM emprunteur c WHERE c.id=0) VALUES((SELECT ref(e) FROM emprunt e WHERE e.id=0));INSERT INTO TABLE( SELECT c.emprunts FROM exemplaire c WHERE c.id=6) VALUES((SELECT ref(e) FROM emprunt e WHERE e.id=0));

-- INSERTION EMPRUNT: tuto cuisto:0
INSERT INTO emprunt VALUES ( emprunt_t(1, TO_DATE('20021116','YYYYMMDD'), TO_DATE('20021125','YYYYMMDD'), (SELECT ref(e) FROM emprunteur e WHERE e.id=0), (SELECT ref(e) FROM exemplaire e WHERE e.id=14) ) );
/
INSERT INTO TABLE( SELECT c.emprunts FROM emprunteur c WHERE c.id=0) VALUES((SELECT ref(e) FROM emprunt e WHERE e.id=1));INSERT INTO TABLE( SELECT c.emprunts FROM exemplaire c WHERE c.id=14) VALUES((SELECT ref(e) FROM emprunt e WHERE e.id=1));

-- INSERTION EMPRUNT: tuto cuisto2:0
INSERT INTO emprunt VALUES ( emprunt_t(2, TO_DATE('20021116','YYYYMMDD'), TO_DATE('20021125','YYYYMMDD'), (SELECT ref(e) FROM emprunteur e WHERE e.id=0), (SELECT ref(e) FROM exemplaire e WHERE e.id=17) ) );
/
INSERT INTO TABLE( SELECT c.emprunts FROM emprunteur c WHERE c.id=0) VALUES((SELECT ref(e) FROM emprunt e WHERE e.id=2));INSERT INTO TABLE( SELECT c.emprunts FROM exemplaire c WHERE c.id=17) VALUES((SELECT ref(e) FROM emprunt e WHERE e.id=2));

-- INSERTION EMPRUNT: lindsey stirling:0
INSERT INTO emprunt VALUES ( emprunt_t(3, TO_DATE('20030215','YYYYMMDD'), TO_DATE('20030220','YYYYMMDD'), (SELECT ref(e) FROM emprunteur e WHERE e.id=0), (SELECT ref(e) FROM exemplaire e WHERE e.id=27) ) );
/
INSERT INTO TABLE( SELECT c.emprunts FROM emprunteur c WHERE c.id=0) VALUES((SELECT ref(e) FROM emprunt e WHERE e.id=3));INSERT INTO TABLE( SELECT c.emprunts FROM exemplaire c WHERE c.id=27) VALUES((SELECT ref(e) FROM emprunt e WHERE e.id=3));

-- INSERTION EMPRUNT: lindsey stirling:0
INSERT INTO emprunt VALUES ( emprunt_t(4, TO_DATE('20040326','YYYYMMDD'), TO_DATE('20040330','YYYYMMDD'), (SELECT ref(e) FROM emprunteur e WHERE e.id=0), (SELECT ref(e) FROM exemplaire e WHERE e.id=27) ) );
/
INSERT INTO TABLE( SELECT c.emprunts FROM emprunteur c WHERE c.id=0) VALUES((SELECT ref(e) FROM emprunt e WHERE e.id=4));INSERT INTO TABLE( SELECT c.emprunts FROM exemplaire c WHERE c.id=27) VALUES((SELECT ref(e) FROM emprunt e WHERE e.id=4));

-- INSERTION EMPRUNT: SQL pour les nuls:0
INSERT INTO emprunt VALUES ( emprunt_t(5, TO_DATE('20121209','YYYYMMDD'), NULL, (SELECT ref(e) FROM emprunteur e WHERE e.id=0), (SELECT ref(e) FROM exemplaire e WHERE e.id=7) ) );
/
INSERT INTO TABLE( SELECT c.emprunts FROM emprunteur c WHERE c.id=0) VALUES((SELECT ref(e) FROM emprunt e WHERE e.id=5));INSERT INTO TABLE( SELECT c.emprunts FROM exemplaire c WHERE c.id=7) VALUES((SELECT ref(e) FROM emprunt e WHERE e.id=5));

-- INSERTION EMPRUNT: La sigification scientiques de la forme des nids d abeilles:0
INSERT INTO emprunt VALUES ( emprunt_t(10, TO_DATE('20121209','YYYYMMDD'), NULL, (SELECT ref(e) FROM emprunteur e WHERE e.id=0), (SELECT ref(e) FROM exemplaire e WHERE e.id=40) ) );
/
INSERT INTO TABLE( SELECT c.emprunts FROM emprunteur c WHERE c.id=0) VALUES((SELECT ref(e) FROM emprunt e WHERE e.id=10));INSERT INTO TABLE( SELECT c.emprunts FROM exemplaire c WHERE c.id=40) VALUES((SELECT ref(e) FROM emprunt e WHERE e.id=10));

-- INSERTION EMPRUNT: harry potter 7:0
INSERT INTO emprunt VALUES ( emprunt_t(11, TO_DATE('20121209','YYYYMMDD'), NULL, (SELECT ref(e) FROM emprunteur e WHERE e.id=0), (SELECT ref(e) FROM exemplaire e WHERE e.id=2) ) );
/
INSERT INTO TABLE( SELECT c.emprunts FROM emprunteur c WHERE c.id=0) VALUES((SELECT ref(e) FROM emprunt e WHERE e.id=11));INSERT INTO TABLE( SELECT c.emprunts FROM exemplaire c WHERE c.id=2) VALUES((SELECT ref(e) FROM emprunt e WHERE e.id=11));

-- INSERTION EMPRUNT: tuto cuisto:0
INSERT INTO emprunt VALUES ( emprunt_t(12, TO_DATE('20050423','YYYYMMDD'), TO_DATE('20050428','YYYYMMDD'), (SELECT ref(e) FROM emprunteur e WHERE e.id=1), (SELECT ref(e) FROM exemplaire e WHERE e.id=14) ) );
/
INSERT INTO TABLE( SELECT c.emprunts FROM emprunteur c WHERE c.id=1) VALUES((SELECT ref(e) FROM emprunt e WHERE e.id=12));INSERT INTO TABLE( SELECT c.emprunts FROM exemplaire c WHERE c.id=14) VALUES((SELECT ref(e) FROM emprunt e WHERE e.id=12));

-- INSERTION EMPRUNT: lindsey stirling:0
INSERT INTO emprunt VALUES ( emprunt_t(13, TO_DATE('20121210','YYYYMMDD'), NULL, (SELECT ref(e) FROM emprunteur e WHERE e.id=1), (SELECT ref(e) FROM exemplaire e WHERE e.id=27) ) );
/
INSERT INTO TABLE( SELECT c.emprunts FROM emprunteur c WHERE c.id=1) VALUES((SELECT ref(e) FROM emprunt e WHERE e.id=13));INSERT INTO TABLE( SELECT c.emprunts FROM exemplaire c WHERE c.id=27) VALUES((SELECT ref(e) FROM emprunt e WHERE e.id=13));

-- INSERTION EMPRUNT: john carter:0
INSERT INTO emprunt VALUES ( emprunt_t(14, TO_DATE('20121205','YYYYMMDD'), NULL, (SELECT ref(e) FROM emprunteur e WHERE e.id=1), (SELECT ref(e) FROM exemplaire e WHERE e.id=32) ) );
/
INSERT INTO TABLE( SELECT c.emprunts FROM emprunteur c WHERE c.id=1) VALUES((SELECT ref(e) FROM emprunt e WHERE e.id=14));INSERT INTO TABLE( SELECT c.emprunts FROM exemplaire c WHERE c.id=32) VALUES((SELECT ref(e) FROM emprunt e WHERE e.id=14));

-- INSERTION EMPRUNT: SQL pour les nuls:1
INSERT INTO emprunt VALUES ( emprunt_t(15, TO_DATE('20121210','YYYYMMDD'), NULL, (SELECT ref(e) FROM emprunteur e WHERE e.id=2), (SELECT ref(e) FROM exemplaire e WHERE e.id=8) ) );
/
INSERT INTO TABLE( SELECT c.emprunts FROM emprunteur c WHERE c.id=2) VALUES((SELECT ref(e) FROM emprunt e WHERE e.id=15));INSERT INTO TABLE( SELECT c.emprunts FROM exemplaire c WHERE c.id=8) VALUES((SELECT ref(e) FROM emprunt e WHERE e.id=15));

-- INSERTION EMPRUNT: tuto cuisto2:0
INSERT INTO emprunt VALUES ( emprunt_t(16, TO_DATE('20121207','YYYYMMDD'), NULL, (SELECT ref(e) FROM emprunteur e WHERE e.id=2), (SELECT ref(e) FROM exemplaire e WHERE e.id=17) ) );
/
INSERT INTO TABLE( SELECT c.emprunts FROM emprunteur c WHERE c.id=2) VALUES((SELECT ref(e) FROM emprunt e WHERE e.id=16));INSERT INTO TABLE( SELECT c.emprunts FROM exemplaire c WHERE c.id=17) VALUES((SELECT ref(e) FROM emprunt e WHERE e.id=16));

-- INSERTION EMPRUNT: La sigification scientiques de la forme des nids d abeilles:0
INSERT INTO emprunt VALUES ( emprunt_t(17, TO_DATE('20021205','YYYYMMDD'), TO_DATE('20021215','YYYYMMDD'), (SELECT ref(e) FROM emprunteur e WHERE e.id=9), (SELECT ref(e) FROM exemplaire e WHERE e.id=40) ) );
/
INSERT INTO TABLE( SELECT c.emprunts FROM emprunteur c WHERE c.id=9) VALUES((SELECT ref(e) FROM emprunt e WHERE e.id=17));INSERT INTO TABLE( SELECT c.emprunts FROM exemplaire c WHERE c.id=40) VALUES((SELECT ref(e) FROM emprunt e WHERE e.id=17));

-- INSERTION EMPRUNT: harry potter 7:1
INSERT INTO emprunt VALUES ( emprunt_t(18, TO_DATE('20121207','YYYYMMDD'), NULL, (SELECT ref(e) FROM emprunteur e WHERE e.id=9), (SELECT ref(e) FROM exemplaire e WHERE e.id=3) ) );
/
INSERT INTO TABLE( SELECT c.emprunts FROM emprunteur c WHERE c.id=9) VALUES((SELECT ref(e) FROM emprunt e WHERE e.id=18));INSERT INTO TABLE( SELECT c.emprunts FROM exemplaire c WHERE c.id=3) VALUES((SELECT ref(e) FROM emprunt e WHERE e.id=18));

-- INSERTION EMPRUNT: rootkit, subverting the windows kernel:0
INSERT INTO emprunt VALUES ( emprunt_t(19, TO_DATE('20021205','YYYYMMDD'), TO_DATE('20021215','YYYYMMDD'), (SELECT ref(e) FROM emprunteur e WHERE e.id=3), (SELECT ref(e) FROM exemplaire e WHERE e.id=11) ) );
/
INSERT INTO TABLE( SELECT c.emprunts FROM emprunteur c WHERE c.id=3) VALUES((SELECT ref(e) FROM emprunt e WHERE e.id=19));INSERT INTO TABLE( SELECT c.emprunts FROM exemplaire c WHERE c.id=11) VALUES((SELECT ref(e) FROM emprunt e WHERE e.id=19));

-- INSERTION EMPRUNT: harry potter 7:2
INSERT INTO emprunt VALUES ( emprunt_t(20, TO_DATE('20121130','YYYYMMDD'), NULL, (SELECT ref(e) FROM emprunteur e WHERE e.id=8), (SELECT ref(e) FROM exemplaire e WHERE e.id=4) ) );
/
INSERT INTO TABLE( SELECT c.emprunts FROM emprunteur c WHERE c.id=8) VALUES((SELECT ref(e) FROM emprunt e WHERE e.id=20));INSERT INTO TABLE( SELECT c.emprunts FROM exemplaire c WHERE c.id=4) VALUES((SELECT ref(e) FROM emprunt e WHERE e.id=20));

-- INSERTION EMPRUNT: rootkit, subverting the windows kernel:0
INSERT INTO emprunt VALUES ( emprunt_t(21, TO_DATE('20021216','YYYYMMDD'), TO_DATE('20021225','YYYYMMDD'), (SELECT ref(e) FROM emprunteur e WHERE e.id=8), (SELECT ref(e) FROM exemplaire e WHERE e.id=11) ) );
/
INSERT INTO TABLE( SELECT c.emprunts FROM emprunteur c WHERE c.id=8) VALUES((SELECT ref(e) FROM emprunt e WHERE e.id=21));INSERT INTO TABLE( SELECT c.emprunts FROM exemplaire c WHERE c.id=11) VALUES((SELECT ref(e) FROM emprunt e WHERE e.id=21));

-- INSERTION EMPRUNT: harry potter 7:3
INSERT INTO emprunt VALUES ( emprunt_t(22, TO_DATE('2012128','YYYYMMDD'), NULL, (SELECT ref(e) FROM emprunteur e WHERE e.id=4), (SELECT ref(e) FROM exemplaire e WHERE e.id=5) ) );
/
INSERT INTO TABLE( SELECT c.emprunts FROM emprunteur c WHERE c.id=4) VALUES((SELECT ref(e) FROM emprunt e WHERE e.id=22));INSERT INTO TABLE( SELECT c.emprunts FROM exemplaire c WHERE c.id=5) VALUES((SELECT ref(e) FROM emprunt e WHERE e.id=22));

-- INSERTION EMPRUNT: harry potter 7:0
INSERT INTO emprunt VALUES ( emprunt_t(23, TO_DATE('20121009','YYYYMMDD'), TO_DATE('20121019','YYYYMMDD'), (SELECT ref(e) FROM emprunteur e WHERE e.id=5), (SELECT ref(e) FROM exemplaire e WHERE e.id=2) ) );
/
INSERT INTO TABLE( SELECT c.emprunts FROM emprunteur c WHERE c.id=5) VALUES((SELECT ref(e) FROM emprunt e WHERE e.id=23));INSERT INTO TABLE( SELECT c.emprunts FROM exemplaire c WHERE c.id=2) VALUES((SELECT ref(e) FROM emprunt e WHERE e.id=23));

-- INSERTION EMPRUNT: SQL pour les nuls:0
INSERT INTO emprunt VALUES ( emprunt_t(24, TO_DATE('20120817','YYYYMMDD'), TO_DATE('20120824','YYYYMMDD'), (SELECT ref(e) FROM emprunteur e WHERE e.id=6), (SELECT ref(e) FROM exemplaire e WHERE e.id=7) ) );
/
INSERT INTO TABLE( SELECT c.emprunts FROM emprunteur c WHERE c.id=6) VALUES((SELECT ref(e) FROM emprunt e WHERE e.id=24));INSERT INTO TABLE( SELECT c.emprunts FROM exemplaire c WHERE c.id=7) VALUES((SELECT ref(e) FROM emprunt e WHERE e.id=24));

-- INSERTION EMPRUNT: harry potter 7:1
INSERT INTO emprunt VALUES ( emprunt_t(25, TO_DATE('20121009','YYYYMMDD'), TO_DATE('20121019','YYYYMMDD'), (SELECT ref(e) FROM emprunteur e WHERE e.id=7), (SELECT ref(e) FROM exemplaire e WHERE e.id=3) ) );
/
INSERT INTO TABLE( SELECT c.emprunts FROM emprunteur c WHERE c.id=7) VALUES((SELECT ref(e) FROM emprunt e WHERE e.id=25));INSERT INTO TABLE( SELECT c.emprunts FROM exemplaire c WHERE c.id=3) VALUES((SELECT ref(e) FROM emprunt e WHERE e.id=25));


