--------------------------------------------------------------------------------
-- 1
--	Liste des documents (avec toutes les info même celles des sous types),
--	par ordre alphabétique des titres
--	dont le thème comprend le mot informatique ou mathématiques
--
SELECT
	DISTINCT e.document.id AS id, e.document.titre AS titre, e.document.theme AS theme, e.document.nbrExpl AS nbrExpl,
	e.document.getType() AS typ,
	e.document.getDuree() AS duree,
	e.document.getNbrPages() AS nbrPages,
	e.document.getNbrSsTitre() AS nbrSsTitre,
	e.document.getFormate() AS formate
FROM
	exemplaire e
WHERE
	LOWER(e.document.theme) LIKE '%info%'
	OR
	LOWER(e.document.theme) LIKE '%math%'
;
/


--------------------------------------------------------------------------------
-- 2
--	Liste (titre et thème) des documents
--	empruntés par Dupont
--	entre le 15/11/2002 et le 15/11/2003
--
SELECT em.exemplaire.document.titre, em.exemplaire.document.theme
FROM emprunt em
WHERE
	UPPER(em.emprunteur.nom) = 'DUPONT'
	AND
	em.dateEmprunt BETWEEN TO_DATE('20021115','yyyymmdd') AND TO_DATE('20031115','yyyymmdd')
;
/


--------------------------------------------------------------------------------
-- 3
--	Pour chaque emprunteur, donner la liste des titres de documents qu'ils a empruntés
--	avec le nom des auteurs pour chaque document
--
-- Format du select: <id emp INT, nom emp VARCHAR2, docInfo:< id exm INT, titre varchar2, auteurs:<...>> >

CREATE OR REPLACE TYPE RAW_book_autor_typ AS OBJECT
(
	id			INT,
	titre		VARCHAR2(100),
	auteurs		ens_ref_auteur
);
/
CREATE OR REPLACE TYPE RAW_book_autor_t AS TABLE OF RAW_book_autor_typ;
/

SELECT
	e.id, e.nom, CAST( MULTISET(SELECT e2.exemplaire.id, e2.exemplaire.document.titre, e2.exemplaire.document.auteurs FROM emprunt e2 WHERE e2.emprunteur.id=e.id) AS RAW_book_autor_t) AS docInfo
FROM
	emprunteur e;
/

-- Suppression des types temporaire
begin
  for r in (select type_name from user_types WHERE type_name IN('RAW_BOOK_AUTOR_T','RAW_BOOK_AUTOR_TYP')) loop
    execute immediate 'drop type ' || r.type_name || ' force';
  end loop;
end;
/


--------------------------------------------------------------------------------
-- 4
--	Noms des auteurs ayant écrit un livre édité chez Dunod.
--	ATTENTION : cette requète est à exécuter sur la base d'un autre binôme
--	qui doit nous autoriser à lire certaines tables(uniquement celles necessaires).
--	Les ordres utilisés sont les suivant :
--
SELECT a.r.nom
FROM livre l, TABLE(l.auteurs) a
WHERE UPPER(l.editeur.nom) = 'DUNOD';
/


--------------------------------------------------------------------------------
-- 5
--	Quantité totale des exemplaires édités chez Eyrolles
--
SELECT COUNT(lod.id) AS nbDocEdited
FROM listOfDocuments lod
WHERE lod.editeur.nom='Eyrolles';
/


--------------------------------------------------------------------------------
-- 6
--	Pour chaque éditeur, nombre de documents présents à la bibliothèque
--
SELECT e.id AS idEditeur, COUNT(docs.r.id) AS nbrDocuments
FROM editeur e, TABLE(e.documents) docs
GROUP BY e.id;
/


--------------------------------------------------------------------------------
-- 7
--	Pour chaque document, nombre de fois où il a été emprunté
--
SELECT COUNT(em.id) AS nbEmprunt, em.exemplaire.document.id AS document_id
FROM emprunt em
GROUP BY em.exemplaire.document.id;
/


--------------------------------------------------------------------------------
-- 8
--	Liste des éditeurs ayant édité plus de 2 documents
--	d'informatique ou de mathématiques
--
-- Note: Nous utilisons LIKE pour des probleme d'encodage des accents. (mathématique)
--
SELECT e1.id
FROM editeur e1, TABLE (e1.documents) docs1
WHERE
	docs1.r.theme LIKE '%math%'
	OR
	docs1.r.theme LIKE '%info%'
GROUP BY e1.id
HAVING COUNT(e1.id) > 2;
/


--------------------------------------------------------------------------------
-- 9
--	Noms des emprunteurs habitants la même adresse que Dupont
--
SELECT e.nom
FROM emprunteur dupn, emprunteur e
WHERE
	LOWER(dupn.nom)='dupont'
	AND
	e.id <> dupn.id
	AND
	e.adresse.ville = dupn.adresse.ville
	AND
	e.adresse.codeP = dupn.adresse.codeP
	AND
	e.adresse.nomRue = dupn.adresse.nomRue
	AND
	e.adresse.numRue = dupn.adresse.numRue
	AND
	(
		((e.adresse.complement = '' OR e.adresse.complement IS NULL) AND (dupn.adresse.complement = '' OR dupn.adresse.complement IS NULL))
		OR
		e.adresse.complement = dupn.adresse.complement
	)
;
/


--------------------------------------------------------------------------------
-- 10
--	Liste des éditeurs n'ayant pas édité de documents d'informatique
--
SELECT e.nom
FROM editeur e
WHERE e.id NOT IN (
	SELECT DISTINCT e.id
	FROM editeur e, TABLE (e.documents) docs
	WHERE docs.r.theme = 'informatique'
);
/


--------------------------------------------------------------------------------
-- 11
--	Noms des personnes n'ayant jamais emprunté de documents
--
-- Version avec Cadinality
SELECT e.nom
FROM emprunteur e
WHERE CARDINALITY(e.emprunts) = 0;
/
-- Version SANS Cadinality
SELECT e.nom
FROM emprunteur e
WHERE (SELECT COUNT(*) FROM emprunt em WHERE em.emprunteur.id=e.id)=0;
/


--------------------------------------------------------------------------------
-- 12
--	Liste des documents n'ayant jamais été empruntés
--
SELECT DISTINCT ld.id, ld.typ, ld.titre, ld.theme, ld.nbrExpl
FROM listOfDocuments ld
WHERE ld.id NOT IN (
	SELECT em.exemplaire.document.id
	FROM emprunt em
);
/


--------------------------------------------------------------------------------
-- 13
--	Donnez la liste des emprunteurs (nom, prénom)
--	appartenant à la catégorie des professionnels
--	ayant emprunté au moins une fois un dvd
--	au cours des 6 derniers mois
--
SELECT DISTINCT em.emprunteur.nom, em.emprunteur.prenom
FROM emprunt em, dvd d, TABLE (d.exemplaires) expls
WHERE
	em.emprunteur.categorie.nom='professionnel'
	AND
	(
		em.dateRendu >= (SYSDATE-31*6)
		OR
		em.dateEmprunt >= (SYSDATE-31*6)
	)
	AND
	em.exemplaire.id = expls.r.id
;
/


--------------------------------------------------------------------------------
-- 14
--	Liste des documents dont le nombre d'exemplaires est supérieur au nombre moyen d'exemplaires
--
-- Suppression de view
begin
	for r in (select view_name from user_views WHERE view_name='NBREXEMPLAIRESPARDOCUMENT') loop
		execute immediate 'drop view ' || r.view_name;
	end loop;
end;
/
CREATE VIEW nbrExemplairesParDocument AS
	SELECT ex.document.id idDoc, COUNT(ex.id) AS nbrExemplaires
	FROM exemplaire ex
	GROUP BY ex.document.id
;
/

SELECT nbrE.idDoc
FROM nbrExemplairesParDocument nbrE
WHERE nbrE.nbrExemplaires > (SELECT ROUND(AVG(n.nbrExemplaires)) AS nb FROM nbrExemplairesParDocument n);
/


--------------------------------------------------------------------------------
-- 15
--	Noms des auteurs ayant écrit des documents d'informatique et de mathématiques
--	ATTENTION : ceux qui ont écrit les deux !!
--
-- Note: Nous utilisons LIKE pour des probleme d'encodage des accents. (mathématique)
--
SELECT DISTINCT a1.r.nom
FROM livre l1, TABLE (l1.auteurs) a1, livre l2, TABLE (l2.auteurs) a2
WHERE
	LOWER(l1.theme) LIKE '%info%'
	AND
	LOWER(l2.theme) LIKE '%math%'
	AND
	a1.r.id = a2.r.id
;
/


--------------------------------------------------------------------------------
-- 16
--	Editeur dont le nombre de documents empruntés est le plus grand
--

-- Suppression de view
begin
	for r in (select view_name from user_views WHERE view_name='EDITEUR_CMP') loop
		execute immediate 'drop view ' || r.view_name;
	end loop;
end;
/

-- Création de view
CREATE VIEW editeur_cmp AS
	SELECT ed.id, COUNT(em.id) AS NBEmprunt
	FROM
		editeur ed, TABLE(ed.documents) doc, emprunt em
	WHERE
		em.exemplaire.id IN (SELECT e.id FROM exemplaire e WHERE e.document.id=doc.r.id)
	GROUP BY ed.id
;
/

-- Selection de l'editeur dont le nombre de documents emprunté est le plus grand.
SELECT ed.id, ed.nom, edc.NBEmprunt
FROM editeur_cmp edc, editeur ed
WHERE
	edc.id = ed.id
	AND
	edc.NBEmprunt = (SELECT MAX(e.NBEmprunt) FROM editeur_cmp e)
;
/


--------------------------------------------------------------------------------
-- 17
--	Liste de documents n'ayant aucun mot-clef en commun avec le document dont le titre est "SQL pour les nuls"
--
-- MULTISET EXCEPT http://docs.oracle.com/cd/B19306_01/server.102/b14200/operators006.htm
-- Version AVEC CARD
SELECT DISTINCT l2.id, l2.titre
FROM livre l1, livre l2
WHERE
	l1.titre='SQL pour les nuls'
	AND
	CARDINALITY(l2.motsCles MULTISET EXCEPT DISTINCT l1.motsCles) = CARDINALITY(l2.motsCles)
;
/

-- Version SANS CARD
SELECT DISTINCT l2.id, l2.titre
FROM livre l1, livre l2
WHERE
	l1.titre='SQL pour les nuls'
	AND
	(SELECT COUNT(*) FROM TABLE(l2.motsCles MULTISET EXCEPT DISTINCT l1.motsCles) med) = (SELECT COUNT(*) FROM TABLE(l2.motsCles) mc)
;
/


--------------------------------------------------------------------------------
-- 18
--	Liste des documents ayant au moins un mot-clef en commun avec le document dont le titre est "SQL pour les nuls"
--
-- Version AVEC CARD
SELECT DISTINCT l2.id, l2.titre
FROM livre l1, livre l2
WHERE
	l1.titre='SQL pour les nuls'
	AND
	CARDINALITY(l2.motsCles MULTISET EXCEPT DISTINCT l1.motsCles) < CARDINALITY(l2.motsCles)
;
/

-- Version SANS CARD
SELECT DISTINCT l2.id, l2.titre
FROM livre l1, livre l2
WHERE
	l1.titre='SQL pour les nuls'
	AND
	(SELECT COUNT(*) FROM TABLE(l2.motsCles MULTISET EXCEPT DISTINCT l1.motsCles) med) < (SELECT COUNT(*) FROM TABLE(l2.motsCles))
;
/


--------------------------------------------------------------------------------
-- 19
--	Liste des documents ayant au moins les mêmes mot-clef que le document dont le titre est "SQL pour les nuls"
--
-- Version AVEC CARD
SELECT DISTINCT l2.id, l2.titre
FROM livre l1, livre l2
WHERE
	l1.titre='SQL pour les nuls'
	AND
	CARDINALITY(l2.motsCles MULTISET EXCEPT DISTINCT l1.motsCles) = CARDINALITY(l2.motsCles)-CARDINALITY(l1.motsCles)
;
/

-- Version SANS CARD
SELECT DISTINCT l2.id, l2.titre
FROM livre l1, livre l2
WHERE
	l1.titre='SQL pour les nuls'
	AND
	(SELECT COUNT(*) FROM TABLE(l2.motsCles MULTISET EXCEPT DISTINCT l1.motsCles) med) = ((SELECT COUNT(*) FROM TABLE(l2.motsCles) mc)-(SELECT COUNT(*) FROM TABLE(l1.motsCles) mc))
;
/


--------------------------------------------------------------------------------
-- 20
--	Liste des documents ayant exactement les mêmes mot-clef que le document dont le titre est "SQL pour les nuls"
--
-- Version AVEC CARD
SELECT l2.id, l2.titre
FROM livre l1, livre l2
WHERE
	l1.titre='SQL pour les nuls'
	AND
	CARDINALITY(l1.motsCles) = CARDINALITY(l2.motsCles)-- On test le nombre de mot clé. EN effet si l2 ne possede qu'un mot cle et l1 2 mot clé et l1 pourrait finir vide alors qu'il ne remplis pas toutes les conditions
	AND
	CARDINALITY(l2.motsCles MULTISET EXCEPT DISTINCT l1.motsCles) = 0
;
/

-- Version SANS CARD
SELECT l2.id, l2.titre
FROM livre l1, livre l2
WHERE
	l1.titre='SQL pour les nuls'
	AND
	(SELECT COUNT(*) FROM TABLE(l1.motsCles) med) = (SELECT COUNT(*) FROM TABLE(l2.motsCles) med)
	AND
	(SELECT COUNT(*) FROM TABLE(l2.motsCles MULTISET EXCEPT DISTINCT l1.motsCles) med) = 0
;
/
