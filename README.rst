======
README
======

:Info: See <https://bitbucket.org/DevsOfLegend/bda1-multimedia> for repo.
:Author: NUEL Guillaume (ImmortalPC) and DECK Léa (dla)
:Date: $Date: 2012-12-21 $
:Revision: $Revision: 95 $
:Description: Base de données objet représantant une bibliotheque. La base de donnée est conçut pour Oracle 11 en SQL3 avec les TYPE, l'héritage, les methodes, ...


**Description**
---------------
Base de données objet représentant une bibliotheque. La base de donnée est conçue pour Oracle 11 en SQL3 avec les TYPE, l'héritage, les methodes, ...
Ce mini projet a été réalisé dans le cadre du Master 1 de Cryptis.

| Ce programme comprend les fonctionnalité suivantes:

- Générateur de données en python.
- Gestion d'une bibliotheque en SQL3 avec les Type et de l'héritage.
- Gestion des champs fils via des méthodes.
- Système de suppression automatique des types/tables/vue/trigger.


**Execution**
---------------
Cette base de données a été conçu en SQL3 sous SQL Developper pour Oracle 11.

- Les fichiers doivent être executés dans **l'ordre**: 1_XXX -> 2_XXX -> ... -> 8_XXX.
- **NE** **PAS** **EXECUTER** le script sur **les** **trigger**. Oracle 11 ne gère pas les triggers avec SQL3.


**Format** **du** **code**
--------------------------
- Le code est en UTF-8
- Le code est indenté avec des tabulations réelles (\\t)
- Le code est prévu pour être affiché avec une taille de 4 pour les tab (\\t)
- Les fins de lignes sont de type LF (\\n)
- IDE utilisé: SQL Developper


**IDE** **RST**
---------------
RST créé grace au visualisateur: http://rst.ninjs.org/
