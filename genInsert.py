#!/usr/bin/python
# coding: utf-8

import random,sys;


data = {
	'livre': {
		0:{
			'titre':'alice au pays des merveilles',
			'theme':'fantastique',
			'nbrExpl': 2,
			'nbrPages':random.randint(20,500),
			'editeur':{
				'nom':'Walt Disney',
				'numTel':'0254145874',
				'adresse':"12, 'rue des ortilles', '', 87000, 'Limoges'"
			},
			'auteurs':{
				0:{
					'nom':'Walt Disney',
					'prenom':'Disney',
					'numTel':'0254145874',
					'dateN':'1918-05-15'
				}
			},
			'motsCles':['Walt Disney','carte','lapin']
		},
		1:{
			'titre':'harry potter 7',
			'theme':'fantastique',
			'nbrExpl': 4,
			'nbrPages':random.randint(20,500),
			'editeur':{
				'nom':'Dunod',
				'numTel':'0555352414',
				'adresse':"2, 'rue des jonquilles', '', 87000, 'Limoges'"
			},
			'auteurs':{
				0:{
					'nom':'J.K.ROWLING',
					'prenom':'Joanne',
					'numTel':'0254145874',
					'dateN':'1918-05-15'
				}
			},
			'motsCles':['sorcier','geant','celuidontonnedoitpasprononcerlenom']
		},
		2:{
			'titre':'le major parlait trop',
			'theme':'policier',
			'nbrExpl': 1,
			'nbrPages':random.randint(20,500),
			'editeur':{
				'nom':'Eyrolles',
				'numTel':'0325211445',
				'adresse':"5, 'rue des lilas', '', 57100, 'thionville'"
			},
			'auteurs':{
				0:{
					'nom':'Chrystie',
					'prenom':'agatha',
					'numTel':'0322514789',
					'dateN':'1921-02-24'
				}
			},
			'motsCles':['mystere','meurtre']
		},
		3:{
			'titre':'SQL pour les nuls',
			'theme':'informatique',
			'nbrExpl': 4,
			'nbrPages':random.randint(20,500),
			'editeur':{
				'nom':'Eyrolles',
				'numTel':'0325211445',
				'adresse':"5, 'rue des lilas', '', 57100, 'thionville'"
			},
			'auteurs':{
				0:{
					'nom':'kenn',
					'prenom':'man',
					'numTel':'0122547895',
					'dateN':'1956-03-15'
				},
				1:{
					'nom':'john',
					'prenom':'mickael',
					'numTel':'0233654896',
					'dateN':'1978-07-15'
				},
				2:{
					'nom':'jordan',
					'prenom':'mickael',
					'numTel':'0214587458',
					'dateN':'1936-05-30'
				}
			},
			'motsCles':['code','tutoriel']
		},
		4:{
			'titre':'rootkit, subverting the windows kernel',
			'theme':'informatique',
			'nbrExpl': 3,
			'nbrPages':random.randint(20,500),
			'editeur':{
				'nom':'Eyrolles',
				'numTel':'0325211445',
				'adresse':"5, 'rue des lilas', '', 57100, 'thionville'"
			},
			'auteurs':{
				0:{
					'nom':'deck',
					'prenom':'Lea',
					'numTel':'0456895847',
					'dateN':'1925-12-15'
				},
				1:{
					'nom':'nuel',
					'prenom':'guillaume',
					'numTel':'0125458745',
					'dateN':'1918-05-15'
				},
				2:{
					'nom':'barry',
					'prenom':'thierno',
					'numTel':'0522114478',
					'dateN':'1918-05-15'
				}
			},
			'motsCles':['tutoriel','code','ring']
		},
		5:{
			'titre':'tuto cuisto',
			'theme':'cuisine',
			'nbrExpl': 3,
			'nbrPages':random.randint(20,500),
			'editeur':{
				'nom':'Cesar',
				'numTel':'0354874589',
				'adresse':"15, 'rue des tourbes', '', 57100, 'thionville'"
			},
			'auteurs':{
				0:{
					'nom':'tavernier',
					'prenom':'sonny',
					'numTel':'0215478549',
					'dateN':'1946-08-25'
				},
				1:{
					'nom':'fleury',
					'prenom':'benoit',
					'numTel':'0214587458',
					'dateN':'1916-10-15'
				}
			},
			'motsCles':['cuisine','tutoriel']
		},
		6:{
			'titre':'tuto cuisto2',
			'theme':'cuisine',
			'nbrExpl': 2,
			'nbrPages':random.randint(20,500),
			'editeur':{
				'nom':'Socrate',
				'numTel':'0566987486',
				'adresse':"7, 'rue des lilolas', '', 58100, 'thionville2'"
			},
			'auteurs':{
				0:{
					'nom':'fleury',
					'prenom':'benoit',
					'numTel':'0214587458',
					'dateN':'1916-10-15'
				}
			},
			'motsCles':['tutoriel']
		},
		7:{
			'titre':'Intelligence artificielle',
			'theme':'mathematiques',
			'nbrExpl': 3,
			'nbrPages':random.randint(20,500),
			'editeur':{
				'nom':'Deck',
				'numTel':'0325211445',
				'adresse':"14, 'rue des paquerette', '', 38440, 'Beauvoir de marc'"
			},
			'auteurs':{
				0:{
					'nom':'kenn',
					'prenom':'man',
					'numTel':'0122547895',
					'dateN':'1956-03-15'
				}
			},
			'motsCles':['robot']
		},
		8:{
			'titre':'intelligence artificielle et algorithmique',
			'theme':'mathematiques et informatiques',
			'nbrExpl': 1,
			'nbrPages':random.randint(20,500),
			'editeur':{
				'nom':'Dupuis',
				'numTel':'0355478951',
				'adresse':"5, 'rue des lipolas', '', 57140, 'noville'"
			},
			'auteurs':{
				0:{
					'nom':'fleury',
					'prenom':'benoit',
					'numTel':'0214587458',
					'dateN':'1916-10-15'
				}
			},
			'motsCles':['intelligence','robot']
		},
		9:{
			'titre':'complexite et calculabitlite côte code',
			'theme':'mathematiques et informatiques',
			'nbrExpl': 1,
			'nbrPages':random.randint(20,500),
			'editeur':{
				'nom':'Nuel',
				'numTel':'0325211445',
				'adresse':"6, 'rue des dionees attrapes mouches', 'appart 8', 67310, 'Wasselonne'"
			},
			'auteurs':{
				0:{
					'nom':'kenn',
					'prenom':'man',
					'numTel':'0122547895',
					'dateN':'1956-03-15'
				}
			},
			'motsCles':['complexite','calculabilite','algorithme']
		}
	},
	'cd':{
		0:{
			'titre':'mozart',
			'theme':'musique classique',
			'nbrExpl': 3,
			'duree':random.randint(20,500),
			'nbr_sstitre':random.randint(0,5),
			'editeur':{
				'nom':'Deck',
				'numTel':'0325211445',
				'adresse':"14, 'rue des paquerette', '', 38440, 'Beauvoir de marc'"
			},
			'auteurs':{
				0:{
					'nom':'barry',
					'prenom':'thierno',
					'numTel':'0522114478',
					'dateN':'1919-05-15'
				}
			},
			'motsCles':['classique']
		},
		1:{
			'titre':'lindsey stirling',
			'theme':'rock',
			'nbrExpl': 1,
			'duree':random.randint(20,500),
			'nbr_sstitre':random.randint(0,5),
			'editeur':{
				'nom':'Eyrolles',
				'numTel':'0325211445',
				'adresse':"5, 'rue des lilas', '', 57100, 'thionville'"
			},
			'auteurs':{
				0:{
					'nom':'john',
					'prenom':'mickael',
					'numTel':'0233654896',
					'dateN':'1978-07-15'
				}
			},
			'motsCles':['violon']
		},
		2:{
			'titre':'metallica',
			'theme':'rock',
			'nbrExpl': 3,
			'duree':random.randint(20,500),
			'nbr_sstitre':random.randint(0,5),
			'editeur':{
				'nom':'Dunod',
				'numTel':'0555352414',
				'adresse':"2, 'rue des jonquilles', '', 87000, 'Limoges'"
			},
			'auteurs':{
				0:{
					'nom':'barry',
					'prenom':'thierno',
					'numTel':'0522114478',
					'dateN':'1919-05-15'
				}
			},
			'motsCles':['rock']
		},
		3:{
			'titre':'epica',
			'theme':'metal synphonique',
			'nbrExpl': 1,
			'duree':random.randint(20,500),
			'nbr_sstitre':random.randint(0,5),
			'editeur':{
				'nom':'Eyrolles',
				'numTel':'0325211445',
				'adresse':"5, 'rue des lilas', '', 57100, 'thionville'"
			},
			'auteurs':{
				0:{
					'nom':'tavernier',
					'prenom':'sonny',
					'numTel':'0215478549',
					'dateN':'1946-08-25'
				}
			},
			'motsCles':['symphonie','live']
		}
	},
	'dvd':{
		0:{
			'titre':'john carter',
			'theme':'fantastique',
			'nbrExpl': 1,
			'duree':random.randint(20,500),
			'editeur':{
				'nom':'Walt Disney',
				'numTel':'0254145874',
				'adresse':"12, 'rue des ortilles', '', 87000, 'Limoges'"
			},
			'auteurs':{
				0:{
					'nom':'Walt Disney',
					'prenom':'Disney',
					'numTel':'0254145874',
					'dateN':'1918-05-15'
				}
			},
			'motsCles':['Walt Disney', 'mars']
		},
		1:{
			'titre':'Apprendre à coder Jquery',
			'theme':'informatique',
			'nbrExpl': 1,
			'duree':random.randint(20,500),
			'editeur':{
				'nom':'Eyrolles',
				'numTel':'0325211445',
				'adresse':"5, 'rue des lilas', '', 57100, 'thionville'"
			},
			'auteurs':{
				0:{
					'nom':'tavernier',
					'prenom':'sonny',
					'numTel':'0215478549',
					'dateN':'1946-08-25'
				}
			},
			'motsCles':['code']
		},
		2:{
			'titre':'C est pas sorcier',
			'theme':'scientifique et informatique',
			'nbrExpl': 1,
			'duree':random.randint(20,500),
			'editeur':{
				'nom':'Deck',
				'numTel':'0325211445',
				'adresse':"14, 'rue des paquerette', '', 38440, 'Beauvoir de marc'"
			},
			'auteurs':{
				0:{
					'nom':'barry',
					'prenom':'thierno',
					'numTel':'0522114478',
					'dateN':'1919-05-15'
				}
			},
			'motsCles':['science','camion']
		},
		3:{
			'titre':'Chérie j ai rétrécis les gosses',
			'theme':'science fiction',
			'nbrExpl': 1,
			'duree':random.randint(20,500),
			'editeur':{
				'nom':'Deck',
				'numTel':'0325211445',
				'adresse':"14, 'rue des paquerette', '', 38440, 'Beauvoir de marc'"
			},
			'auteurs':{
				0:{
					'nom':'barry',
					'prenom':'thierno',
					'numTel':'0522114478',
					'dateN':'1919-05-15'
				}
			},
			'motsCles':['minuscule']
		}
	},
	'video':{
		0:{
			'titre':'Tarte et gateaux',
			'theme':'tutoriel de cuisine',
			'nbrExpl': 2,
			'duree':random.randint(20,500),
			'formate':'AVI',
			'editeur':{
				'nom':'Eyrolles',
				'numTel':'0325211445',
				'adresse':"5, 'rue des lilas', '', 57100, 'thionville'"
			},
			'auteurs':{
				0:{
					'nom':'john',
					'prenom':'mickael',
					'numTel':'0233654896',
					'dateN':'1978-07-15'
				}
			},
			'motsCles':['plat']
		},
		1:{
			'titre':'hacker et Cie',
			'theme':'korben info : securite informatique',
			'nbrExpl': 2,
			'duree':random.randint(20,500),
			'formate':'AVI',
			'editeur':{
				'nom':'Eyrolles',
				'numTel':'0325211445',
				'adresse':"5, 'rue des lilas', '', 57100, 'thionville'"
			},
			'auteurs':{
				0:{
					'nom':'john',
					'prenom':'mickael',
					'numTel':'0233654896',
					'dateN':'1978-07-15'
				}
			},
			'motsCles':['hook']
		},
		2:{
			'titre':'La sigification scientiques de la forme des nids d abeilles',
			'theme':'scientifique et mathematiques',
			'nbrExpl': 1,
			'duree':random.randint(20,500),
			'formate':'AVI',
			'editeur':{
				'nom':'Eyrolles',
				'numTel':'0325211445',
				'adresse':"5, 'rue des lilas', '', 57100, 'thionville'"
			},
			'auteurs':{
				0:{
					'nom':'john',
					'prenom':'mickael',
					'numTel':'0233654896',
					'dateN':'1978-07-15'
				}
			},
			'motsCles':['rond','regle','stylo']
		}
	},

	'categorie':{
		0:{
			'nom':'personnel',
			'dureeLivre':30,
			'dureeCD':21,
			'dureeDVD':21,
			'dureeVideo':21,
			'nbMaxEmprunt':10
		},
		1:{
			'nom':'professionnel',
			'dureeLivre':30,
			'dureeCD':14,
			'dureeDVD':14,
			'dureeVideo':14,
			'nbMaxEmprunt':5
		},
		2:{
			'nom':'public',
			'dureeLivre':15,
			'dureeCD':7,
			'dureeDVD':5,
			'dureeVideo':7,
			'nbMaxEmprunt':3
		}
	},
	'emprunteur':{
		0:{
			'nom':'dupont',
			'numTel':'0524157825',
			'prenom':'titi',
			'adresse':"12, 'rue des ortilles', '', 87000, 'Limoges'",
			'categorie': 1
		},
		1:{
			'nom':'heury',
			'numTel':'0214587459',
			'prenom':'tat',
			'adresse':"12, 'rue des ortilles', '', 87000, 'Limoges'",
			'categorie': 1
		},
		2:{
			'nom':'hatchoum',
			'numTel':'0356847512',
			'prenom':'ouioui',
			'adresse':"12, 'rue des ortilles', '', 87000, 'Limoges'",
			'categorie': 2
		},
		3:{
			'nom':'niourem',
			'numTel':'0356847512',
			'prenom':'ahahaha',
			'adresse':"2, 'rue des mistrals gagnants', '', 87000, 'Limoges'",
			'categorie': 2
		},
		4:{
			'nom':'thieury',
			'numTel':'0214587458',
			'prenom':'toto',
			'adresse':"8, 'rue des parapluies', '', 87000, 'Limoges'",
			'categorie': 2
		},
		5:{
			'nom':'jeury',
			'numTel':'0214587451',
			'prenom':'ami',
			'adresse':"6, 'rue des grenouilles', '', 87000, 'Limoges'",
			'categorie': 2
		},
		6:{
			'nom':'mouru',
			'numTel':'0214587459',
			'prenom':'mamo',
			'adresse':"8, 'rue des serpent', '', 87000, 'Limoges'",
			'categorie': 1
		},
		7:{
			'nom':'nemnem',
			'numTel':'0356847512',
			'prenom':'teletobise',
			'adresse':"7, 'rue des nexus castrus', '', 87000, 'Limoges'",
			'categorie': 1
		},
		8:{
			'nom':'miamiam',
			'numTel':'0356847512',
			'prenom':'pikatchou',
			'adresse':"16, 'rue des tiranosaure', '', 87000, 'Limoges'",
			'categorie': 1
		},
		9:{
			'nom':'bijoux',
			'numTel':'0356847512',
			'prenom':'pc',
			'adresse':"16, 'rue des tiranosaure', '', 87000, 'Limoges'",
			'categorie': 2
		},
		10:{
			'nom':'marre',
			'numTel':'0214587451',
			'prenom':'tita',
			'adresse':"3, 'rue des coq', '', 87000, 'Limoges'",
			'categorie': 2
		},
		11:{
			'nom':'flemme',
			'numTel':'0214587458',
			'prenom':'tito',
			'adresse':"49, 'rue des galets', 'appartement 10', 87000, 'Limoges'",
			'categorie': 2
		},
		12:{
			'nom':'petage de cable',
			'numTel':'0214587458',
			'prenom':'tramouet',
			'adresse':"2, 'rue des requins', '', 87000, 'Limoges'",
			'categorie': 2
		},
		13:{
			'nom':'guichour',
			'numTel':'0145874589',
			'prenom':'mouchoir',
			'adresse':"7, 'rue des cepes', '', 87000, 'Limoges'",
			'categorie': 2
		},
		14:{
			'nom':'bijour',
			'numTel':'0356847512',
			'prenom':'ram',
			'adresse':"59, 'rue des bolets', '', 87000, 'Limoges'",
			'categorie': 1
		}
	},

	'emprunt':{
		0:{
			'emprunteur':'dupont',
			'dateEmprunt':'20011203',
			'dateRendu':'20011210',
			'exemplaire':'le major parlait trop:0'
		},
		1:{
			'emprunteur':'dupont',
			'dateEmprunt':'20021116',
			'dateRendu':'20021125',
			'exemplaire':'tuto cuisto:0'
		},
		2:{
			'emprunteur':'dupont',
			'dateEmprunt':'20021116',
			'dateRendu':'20021125',
			'exemplaire':'tuto cuisto2:0'
		},
		3:{
			'emprunteur':'dupont',
			'dateEmprunt':'20030215',
			'dateRendu':'20030220',
			'exemplaire':'lindsey stirling:0'
		},
		4:{
			'emprunteur':'dupont',
			'dateEmprunt':'20040326',
			'dateRendu':'20040330',
			'exemplaire':'lindsey stirling:0'
		},
		5:{
			'emprunteur':'dupont',
			'dateEmprunt':'20121209',
			'dateRendu':'NULL',
			'exemplaire':'SQL pour les nuls:0'
		},
		10:{
			'emprunteur':'dupont',
			'dateEmprunt':'20121209',
			'dateRendu':'NULL',
			'exemplaire':'La sigification scientiques de la forme des nids d abeilles:0'
		},
		11:{
			'emprunteur':'dupont',
			'dateEmprunt':'20121209',
			'dateRendu':'NULL',
			'exemplaire':'harry potter 7:0'
		},
		12:{
			'emprunteur':'heury',
			'dateEmprunt':'20050423',
			'dateRendu':'20050428',
			'exemplaire':'tuto cuisto:0'
		},
		13:{
			'emprunteur':'heury',
			'dateEmprunt':'20121210',
			'dateRendu':'NULL',
			'exemplaire':'lindsey stirling:0'
		},
		14:{
			'emprunteur':'heury',
			'dateEmprunt':'20121205',
			'dateRendu':'NULL',
			'exemplaire':'john carter:0'
		},
		15:{
			'emprunteur':'hatchoum',
			'dateEmprunt':'20121210',
			'dateRendu':'NULL',
			'exemplaire':'SQL pour les nuls:1'
		},
		16:{
			'emprunteur':'hatchoum',
			'dateEmprunt':'20121207',
			'dateRendu':'NULL',
			'exemplaire':'tuto cuisto2:0'
		},
		17:{
			'emprunteur':'bijoux',
			'dateEmprunt':'20021205',
			'dateRendu':'20021215',
			'exemplaire':'La sigification scientiques de la forme des nids d abeilles:0'
		},
		18:{
			'emprunteur':'bijoux',
			'dateEmprunt':'20121207',
			'dateRendu':'NULL',
			'exemplaire':'harry potter 7:1'
		},
		19:{
			'emprunteur':'niourem',
			'dateEmprunt':'20021205',
			'dateRendu':'20021215',
			'exemplaire':'rootkit, subverting the windows kernel:0'
		},
		20:{
			'emprunteur':'miamiam',
			'dateEmprunt':'20121130',
			'dateRendu':'NULL',
			'exemplaire':'harry potter 7:2'
		},
		21:{
			'emprunteur':'miamiam',
			'dateEmprunt':'20021216',
			'dateRendu':'20021225',
			'exemplaire':'rootkit, subverting the windows kernel:0'
		},
		22:{
			'emprunteur':'thieury',
			'dateEmprunt':'2012128',
			'dateRendu':'NULL',
			'exemplaire':'harry potter 7:3'
		},
		23:{
			'emprunteur':'jeury',
			'dateEmprunt':'20121009',
			'dateRendu':'20121019',
			'exemplaire':'harry potter 7:0'
		},
		24:{
			'emprunteur':'mouru',
			'dateEmprunt':'20120817',
			'dateRendu':'20120824',
			'exemplaire':'SQL pour les nuls:0'
		},
		25:{
			'emprunteur':'nemnem',
			'dateEmprunt':'20121009',
			'dateRendu':'20121019',
			'exemplaire':'harry potter 7:1'
		}
	}
};


lst_motsCles = {};
lst_auteurs = {};
lst_editeurs = {};
lst_exemplaires = {};
exemplaire_id = 0;
sql = '';

## Permet de gerer des mot cle
# @param[in] motCle		{String} Le mot cle a ajoute.
# @note Ne sera as ajoute si le motcle est dejà dans la liste
def addKeyWord( motCle ):
	global lst_motsCles, sql;
	if lst_motsCles.has_key(motCle):
		return None;
	lst_motsCles[motCle] = len(lst_motsCles);
	sql += "INSERT INTO mot_cle VALUES ( mot_cle_t("+str(lst_motsCles[motCle])+", '"+motCle+"', ens_ref_document()) );\n/\n";


## Permet de gerer les auteurs
# @param[in] auteur		{String} L'auteur a ajoute.
# @note Ne sera as ajoute si l'auteur est dejà dans la liste
def addAutor( auteur ):
	global lst_auteurs, sql;
	if lst_auteurs.has_key(auteur['nom']):
		return None;
	lst_auteurs[auteur['nom']] = len(lst_auteurs);
	sql += "INSERT INTO auteur VALUES ( auteur_t("+str(lst_auteurs[auteur['nom']])+", '"+auteur['nom']+"', "+auteur['numTel']+", '"+auteur['prenom']+"', TO_DATE('"+auteur['dateN']+"','yyyy-mm-dd'), ens_ref_document()) );\n/\n";


## Permet de gerer les editeurs
# @param[in] editeur		{String} L'auteur a ajoute.
# @note Ne sera as ajoute si l'auteur est dejà dans la liste
def addEditor( editeur ):
	global lst_editeurs, sql;
	if lst_editeurs.has_key(editeur['nom']):
		return None;
	lst_editeurs[editeur['nom']] = len(lst_editeurs);
	sql += "INSERT INTO editeur VALUES ( editeur_t("+str(lst_editeurs[editeur['nom']])+", '"+editeur['nom']+"', "+editeur['numTel']+", adresse_t("+editeur['adresse']+"), ens_ref_document()) );\n/\n";


## Permet d'obtenir l'id d'un user en fonction de son nom
# @param[in] user		{String} Nom du user
# @return {int} Si tout OK, NONE si pas trouve
def getUserId( user ):
	for uid in data['emprunteur']:
		if data['emprunteur'][uid]['nom'] == user:
			return uid;
	print user+' introuvable';
	sys.exit(42);

# toto:42
def getExemplaireID( title ):
	if not lst_exemplaires.has_key(title):
		print('Pas d exemplaire pour '+title);
		sys.exit(42);
	return lst_exemplaires[title];


################################################################################
# LIVRE
typ = 'livre';
for sql_id in data[typ]:
	sql += "-- INSERTION LIVRE: "+data[typ][sql_id]['titre']+"\n";

	# Ajout des mots cle
	for motCle in data[typ][sql_id]['motsCles']:
		addKeyWord(motCle);

	# Ajout des auteurs
	for auteur in data[typ][sql_id]['auteurs'].values():
		addAutor(auteur);

	# Ajout de l'editeur
	addEditor(data[typ][sql_id]['editeur']);

	sql += "INSERT INTO livre VALUES ( livre_t("+str(sql_id)+", '"+data[typ][sql_id]['titre']+"', '"+data[typ][sql_id]['theme']+"', "+str(data[typ][sql_id]['nbrExpl'])+", (SELECT ref(e) FROM editeur e WHERE e.id="+str(lst_editeurs[data[typ][sql_id]['editeur']['nom']])+" ), ens_ref_auteur(), ens_ref_mot_cle(), ens_ref_exemplaire(), "+str(data[typ][sql_id]['nbrPages'])+") );\n/\n";

	# Jointure des mots cle
	for motCle in data[typ][sql_id]['motsCles']:
		sql += "INSERT INTO TABLE(SELECT l.motsCles FROM livre l WHERE l.id="+str(sql_id)+") VALUES ( (SELECT ref(m) FROM mot_cle m WHERE m.id="+str(lst_motsCles[motCle])+") );\n/\n";
		sql += "INSERT INTO TABLE(SELECT m.documents FROM mot_Cle m WHERE m.id="+str(lst_motsCles[motCle])+") VALUES ( (SELECT ref(l) FROM livre l WHERE l.id="+str(sql_id)+") );\n/\n";

	# Jointure des auteurs
	for auteur in data[typ][sql_id]['auteurs'].values():
		sql += "INSERT INTO TABLE(SELECT l.auteurs FROM livre l WHERE l.id="+str(sql_id)+") VALUES ( (SELECT ref(a) FROM auteur a WHERE a.id="+str(lst_auteurs[auteur['nom']])+") );\n/\n";
		sql += "INSERT INTO TABLE(SELECT a.documents FROM auteur a WHERE a.id="+str(lst_auteurs[auteur['nom']])+") VALUES ( (SELECT ref(l) FROM livre l WHERE l.id="+str(sql_id)+") );\n/\n";

	# Generation des exemplaires
	for i in range(data[typ][sql_id]['nbrExpl']):
		lst_exemplaires[data[typ][sql_id]['titre']+':'+str(i)] = exemplaire_id;
		sql += "INSERT INTO exemplaire VALUES ( exemplaire_t("+str(exemplaire_id)+", 1, (select ref(doc) FROM livre doc where doc.id="+str(sql_id)+"), ens_ref_emprunt()) );\n/\n";
		sql += "INSERT INTO TABLE(SELECT l.exemplaires FROM livre l WHERE l.id="+str(sql_id)+") VALUES((SELECT ref(ex) FROM exemplaire ex WHERE ex.id="+str(exemplaire_id)+"));\n/\n";
		exemplaire_id+=1;

	# Jointure editeur
	sql += "INSERT INTO TABLE(SELECT e.documents FROM editeur e WHERE e.id="+str(lst_editeurs[data[typ][sql_id]['editeur']['nom']])+") VALUES((SELECT ref(doc) FROM livre doc WHERE doc.id="+str(sql_id)+"));\n/\n";

	sql += "\n\n";


################################################################################
# CD
typ = 'cd';
for sql_id in data[typ]:
	sql += "-- INSERTION CD: "+data[typ][sql_id]['titre']+"\n";

	# Ajout des mots cle
	for motCle in data[typ][sql_id]['motsCles']:
		addKeyWord(motCle);

	# Ajout des auteurs
	for auteur in data[typ][sql_id]['auteurs'].values():
		addAutor(auteur);

	# Ajout de l'editeur
	addEditor(data[typ][sql_id]['editeur']);

	sql += "INSERT INTO cd VALUES ( cd_t("+str(sql_id)+", '"+data[typ][sql_id]['titre']+"', '"+data[typ][sql_id]['theme']+"', "+str(data[typ][sql_id]['nbrExpl'])+", (SELECT ref(e) FROM editeur e WHERE e.id="+str(lst_editeurs[data[typ][sql_id]['editeur']['nom']])+" ), ens_ref_auteur(), ens_ref_mot_cle(), ens_ref_exemplaire(), "+str(data[typ][sql_id]['duree'])+", "+str(data[typ][sql_id]['nbr_sstitre'])+") );\n/\n";

	# Jointure des mots cle
	for motCle in data[typ][sql_id]['motsCles']:
		sql += "INSERT INTO TABLE(SELECT c.motsCles FROM cd c WHERE c.id="+str(sql_id)+") VALUES ( (SELECT ref(m) FROM mot_cle m WHERE m.id="+str(lst_motsCles[motCle])+") );\n/\n";
		sql += "INSERT INTO TABLE(SELECT m.documents FROM mot_Cle m WHERE m.id="+str(lst_motsCles[motCle])+") VALUES ( (SELECT ref(c) FROM cd c WHERE c.id="+str(sql_id)+") );\n/\n";

	# Jointure des auteurs
	for auteur in data[typ][sql_id]['auteurs'].values():
		sql += "INSERT INTO TABLE(SELECT c.auteurs FROM cd c WHERE c.id="+str(sql_id)+") VALUES ( (SELECT ref(a) FROM auteur a WHERE a.id="+str(lst_auteurs[auteur['nom']])+") );\n/\n";
		sql += "INSERT INTO TABLE(SELECT a.documents FROM auteur a WHERE a.id="+str(lst_auteurs[auteur['nom']])+") VALUES ( (SELECT ref(c) FROM cd c WHERE c.id="+str(sql_id)+") );\n/\n";

	# Generation des exemplaires
	for i in range(data[typ][sql_id]['nbrExpl']):
		lst_exemplaires[data[typ][sql_id]['titre']+':'+str(i)] = exemplaire_id;
		sql += "INSERT INTO exemplaire VALUES ( exemplaire_t("+str(exemplaire_id)+", 1, (select ref(doc) FROM cd doc where doc.id="+str(sql_id)+"), ens_ref_emprunt()) );\n/\n";
		sql += "INSERT INTO TABLE(SELECT c.exemplaires FROM cd c WHERE c.id="+str(sql_id)+") VALUES((SELECT ref(ex) FROM exemplaire ex WHERE ex.id="+str(exemplaire_id)+"));\n/\n";
		exemplaire_id+=1;

	# Jointure editeur
	sql += "INSERT INTO TABLE(SELECT e.documents FROM editeur e WHERE e.id="+str(lst_editeurs[data[typ][sql_id]['editeur']['nom']])+") VALUES((SELECT ref(doc) FROM cd doc WHERE doc.id="+str(sql_id)+"));\n/\n";

	sql += "\n\n";


################################################################################
# DVD
typ = 'dvd';
for sql_id in data[typ]:
	sql += "-- INSERTION DVD: "+data[typ][sql_id]['titre']+"\n";

	# Ajout des mots cle
	for motCle in data[typ][sql_id]['motsCles']:
		addKeyWord(motCle);

	# Ajout des auteurs
	for auteur in data[typ][sql_id]['auteurs'].values():
		addAutor(auteur);

	# Ajout de l'editeur
	addEditor(data[typ][sql_id]['editeur']);

	sql += "INSERT INTO dvd VALUES ( media_t("+str(sql_id)+", '"+data[typ][sql_id]['titre']+"', '"+data[typ][sql_id]['theme']+"', "+str(data[typ][sql_id]['nbrExpl'])+", (SELECT ref(e) FROM editeur e WHERE e.id="+str(lst_editeurs[data[typ][sql_id]['editeur']['nom']])+" ), ens_ref_auteur(), ens_ref_mot_cle(), ens_ref_exemplaire(), "+str(data[typ][sql_id]['duree'])+") );\n/\n";

	# Jointure des mots cle
	for motCle in data[typ][sql_id]['motsCles']:
		sql += "INSERT INTO TABLE(SELECT c.motsCles FROM dvd c WHERE c.id="+str(sql_id)+") VALUES ( (SELECT ref(m) FROM mot_cle m WHERE m.id="+str(lst_motsCles[motCle])+" ));\n/\n";
		sql += "INSERT INTO TABLE(SELECT m.documents FROM mot_Cle m WHERE m.id="+str(lst_motsCles[motCle])+") VALUES ( (SELECT ref(c) FROM dvd c WHERE c.id="+str(sql_id)+" ));\n/\n";

	# Jointure des auteurs
	for auteur in data[typ][sql_id]['auteurs'].values():
		sql += "INSERT INTO TABLE(SELECT c.auteurs FROM dvd c WHERE c.id="+str(sql_id)+") VALUES ( (SELECT ref(a) FROM auteur a WHERE a.id="+str(lst_auteurs[auteur['nom']])+" ));\n/\n";
		sql += "INSERT INTO TABLE(SELECT a.documents FROM auteur a WHERE a.id="+str(lst_auteurs[auteur['nom']])+") VALUES ( (SELECT ref(c) FROM dvd c WHERE c.id="+str(sql_id)+" ));\n/\n";

	# Generation des exemplaires
	for i in range(data[typ][sql_id]['nbrExpl']):
		lst_exemplaires[data[typ][sql_id]['titre']+':'+str(i)] = exemplaire_id;
		sql += "INSERT INTO exemplaire VALUES ( exemplaire_t("+str(exemplaire_id)+", 1, (select ref(doc) FROM dvd doc where doc.id="+str(sql_id)+"), ens_ref_emprunt()) );\n/\n";
		sql += "INSERT INTO TABLE(SELECT c.exemplaires FROM dvd c WHERE c.id="+str(sql_id)+") VALUES((SELECT ref(ex) FROM exemplaire ex WHERE ex.id="+str(exemplaire_id)+"));\n/\n";
		exemplaire_id+=1;

	# Jointure editeur
	sql += "INSERT INTO TABLE(SELECT e.documents FROM editeur e WHERE e.id="+str(lst_editeurs[data[typ][sql_id]['editeur']['nom']])+") VALUES((SELECT ref(doc) FROM dvd doc WHERE doc.id="+str(sql_id)+"));\n/\n";

	sql += "\n\n";


################################################################################
# VIDEO
typ = 'video';
for sql_id in data[typ]:
	sql += "-- INSERTION VIDEO: "+data[typ][sql_id]['titre']+"\n";

	# Ajout des mots cle
	for motCle in data[typ][sql_id]['motsCles']:
		addKeyWord(motCle);

	# Ajout des auteurs
	for auteur in data[typ][sql_id]['auteurs'].values():
		addAutor(auteur);

	# Ajout de l'editeur
	addEditor(data[typ][sql_id]['editeur']);

	sql += "INSERT INTO video VALUES ( video_t("+str(sql_id)+", '"+data[typ][sql_id]['titre']+"', '"+data[typ][sql_id]['theme']+"', "+str(data[typ][sql_id]['nbrExpl'])+", (SELECT ref(e) FROM editeur e WHERE e.id="+str(lst_editeurs[data[typ][sql_id]['editeur']['nom']])+" ), ens_ref_auteur(), ens_ref_mot_cle(), ens_ref_exemplaire(), "+str(data[typ][sql_id]['duree'])+", '"+str(data[typ][sql_id]['formate'])+"') );\n/\n";

	# Jointure des mots cle
	for motCle in data[typ][sql_id]['motsCles']:
		sql += "INSERT INTO TABLE(SELECT c.motsCles FROM video c WHERE c.id="+str(sql_id)+") VALUES ( (SELECT ref(m) FROM mot_cle m WHERE m.id="+str(lst_motsCles[motCle])+") );\n/\n";
		sql += "INSERT INTO TABLE(SELECT m.documents FROM mot_Cle m WHERE m.id="+str(lst_motsCles[motCle])+") VALUES ( (SELECT ref(c) FROM video c WHERE c.id="+str(sql_id)+") );\n/\n";

	# Jointure des auteurs
	for auteur in data[typ][sql_id]['auteurs'].values():
		sql += "INSERT INTO TABLE(SELECT c.auteurs FROM video c WHERE c.id="+str(sql_id)+") VALUES ( (SELECT ref(a) FROM auteur a WHERE a.id="+str(lst_auteurs[auteur['nom']])+") );\n/\n";
		sql += "INSERT INTO TABLE(SELECT a.documents FROM auteur a WHERE a.id="+str(lst_auteurs[auteur['nom']])+") VALUES ( (SELECT ref(c) FROM video c WHERE c.id="+str(sql_id)+") );\n/\n";

	# Generation des exemplaires
	for i in range(data[typ][sql_id]['nbrExpl']):
		lst_exemplaires[data[typ][sql_id]['titre']+':'+str(i)] = exemplaire_id;
		sql += "INSERT INTO exemplaire VALUES ( exemplaire_t("+str(exemplaire_id)+", 1, (select ref(doc) FROM video doc where doc.id="+str(sql_id)+"), ens_ref_emprunt()) );\n/\n";
		sql += "INSERT INTO TABLE(SELECT c.exemplaires FROM video c WHERE c.id="+str(sql_id)+") VALUES((SELECT ref(ex) FROM exemplaire ex WHERE ex.id="+str(exemplaire_id)+"));\n/\n";
		exemplaire_id+=1;

	# Jointure editeur
	sql += "INSERT INTO TABLE(SELECT e.documents FROM editeur e WHERE e.id="+str(lst_editeurs[data[typ][sql_id]['editeur']['nom']])+") VALUES((SELECT ref(doc) FROM video doc WHERE doc.id="+str(sql_id)+"));\n/\n";

	sql += "\n\n";


################################################################################
# CATEGORIE
typ = 'categorie';
for sql_id in data[typ]:
	sql += "-- INSERTION CATEGORIE: "+data[typ][sql_id]['nom']+"\n";
	sql += "INSERT INTO categorie_empr VALUES (categorie_empr_t("+str(sql_id)+", '"+data[typ][sql_id]['nom']+"', "+str(data[typ][sql_id]['nbMaxEmprunt'])+", "+str(data[typ][sql_id]['dureeLivre'])+", "+str(data[typ][sql_id]['dureeCD'])+", "+str(data[typ][sql_id]['dureeDVD'])+", "+str(data[typ][sql_id]['dureeVideo'])+", ens_ref_emprunteur()));\n/\n";
	sql += "\n\n";


################################################################################
# EMPRUNTEUR
typ = 'emprunteur';
for sql_id in data[typ]:
	sql += "-- INSERTION EMPRUNTEUR: "+data[typ][sql_id]['nom']+"\n";
	sql += "INSERT INTO emprunteur VALUES ( emprunteur_t("+str(sql_id)+", '"+data[typ][sql_id]['nom']+"', "+data[typ][sql_id]['numTel']+", '"+data[typ][sql_id]['prenom']+"', adresse_t("+data[typ][sql_id]['adresse']+"), (SELECT ref(c) FROM categorie_empr c WHERE c.id="+str(data[typ][sql_id]['categorie'])+"), ens_ref_emprunt()) );\n/\n";
	sql += "INSERT INTO TABLE( SELECT c.emprunteurs FROM categorie_empr c WHERE c.id="+str(data[typ][sql_id]['categorie'])+") VALUES((SELECT ref(e) FROM emprunteur e WHERE e.id="+str(sql_id)+"));";
	sql += "\n\n";


################################################################################
# EMPRUNT
typ = 'emprunt';
for sql_id in data[typ]:
	sql += "-- INSERTION EMPRUNT: "+data[typ][sql_id]['exemplaire']+"\n";
	if data[typ][sql_id]['dateRendu'] != 'NULL':
		data[typ][sql_id]['dateRendu'] = "TO_DATE('"+data[typ][sql_id]['dateRendu']+"','YYYYMMDD')";
	sql += "INSERT INTO emprunt VALUES ( emprunt_t("+str(sql_id)+", TO_DATE('"+data[typ][sql_id]['dateEmprunt']+"','YYYYMMDD'), "+data[typ][sql_id]['dateRendu']+", (SELECT ref(e) FROM emprunteur e WHERE e.id="+str(getUserId(data[typ][sql_id]['emprunteur']))+"), (SELECT ref(e) FROM exemplaire e WHERE e.id="+str(getExemplaireID(data[typ][sql_id]['exemplaire']))+") ) );\n/\n";
	sql += "INSERT INTO TABLE( SELECT c.emprunts FROM emprunteur c WHERE c.id="+str(getUserId(data[typ][sql_id]['emprunteur']))+") VALUES((SELECT ref(e) FROM emprunt e WHERE e.id="+str(sql_id)+"));";
	sql += "INSERT INTO TABLE( SELECT c.emprunts FROM exemplaire c WHERE c.id="+str(getExemplaireID(data[typ][sql_id]['exemplaire']))+") VALUES((SELECT ref(e) FROM emprunt e WHERE e.id="+str(sql_id)+"));";
	sql += "\n\n";

print sql;
